package net.keyboard.poc.kibo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Duy Dang on 1/22/2016.
 */

// This variable remembers the x co-ordinate value of the touch ACTION_DOWN event
public class RoundedRectanglesWrapperView extends RelativeLayout implements TouchCoordinator  {

    // Debugging TAG
    private static final String TAG = "Debugging";

    // This is the callback to KiboIME
    OnKiboActionListener mOnKiboActionListener;

    TextView wrapper_background;

    KiboView parent;

    TextView[] suggestionViewsLeft;
    TextView[] suggestionViewsRight;

    float maxX = -1, maxY = -1;

    static boolean inPositionControlMode = false;

    static Boolean roundedRectanglesWrapperToInterceptEvents = null;
    static float firstRawX;

    // This variable remembers the y co-ordinate value of the touch ACTION_DOWN event
    static float firstRawY;

    static float firstY;

    // This variable remembers the last position of the finger touch on the screen that moves the cursor
    static float previousCursorMovingTouchPosition;

    // This variable contains the distance required for the finger touch move in order to move the cursor to the right one character
    static float moveDistanceToMoveCursor;

    // if swipe past this limit, eclipseBarsWrapper starts intercepting touches
    static float roundedRectanglesWrapperInterceptLimit;

    // This variable remembers the x co-ordinate value of the previous touch ACTION_MOVE event
    private int maxDelayForCursorMoveRepeating;
    private int minDelayForCursorMoveRepeating;
    private float swipeZoneLength_forMinDelayCursorMoveRepeating;
    private float swipeZoneLength_forMaxDelayCursorMoveRepeating;


    LayoutInflater mInflater;

    // This variable shows the row that the horizontal swipe is locked to
    int swipeLockedRow = -1;

    // This variable locks if row 2 (count from 0) is swiped left when cursorLeft is equal to cursorRight
    boolean lockedSelectionLeftward = false;
    boolean lockedSelectionRightward = false;

    // Store starting time of ACTION_DOWN
    public long actionDownTime = 0;
    public long actionUpTime = 0;

    private int tap_time_limit;
    private int doubleTap_time_limit;

    private float letter_tapLength_maxThreshold = 0.1f;

    private int swipe_time_limit, horizontal_swipe_time_limit;

    public boolean isTapThePreviousAction = false;

    int[] previouslyTappedRoundedRectangleView = null;

    static RoundedRectangleView.LetterAndCursorMoveRepeatingThread letterAndCursorMoveRepeatingThread;

    float firstX_viewPosition_parentRelative = -1;
    float firstY_viewPosition_parentRelative = -1;

    public String lastTappedButton = "";

    float newX, newY;

    float kibo_wrapper_view_bottom_padding;

    float kibo_height;

    public void setLastTappedButton(String input) {

        lastTappedButton = input;

    }

    public String getLastTappedButton() {
        return lastTappedButton;
    }


    public void setIsTapThePreviousAction(boolean input) {

        isTapThePreviousAction = input;

    }

    public boolean getIsTapThePreviousAction() {
        return isTapThePreviousAction;
    }

    public void setPreviouslyTappedRoundedRectangleView(int[] row_column) {

        previouslyTappedRoundedRectangleView = row_column;

    }

    public int[] getPreviouslyTappedRoundedRectangleView() {

        return previouslyTappedRoundedRectangleView;

    }

    public void setActionUpTime(long input) {

        actionUpTime = input;

    }

    public long getActionUpTime() {

        return actionUpTime;

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {

        // Get the current x co-ordinate value of the point the finger touches the screen
        float rawX = event.getRawX();

        // Get the current x co-ordinate value of the point the finger touches the screen
        float rawY = event.getRawY();
        float y = event.getY();

        // Get the horizontal length of the swipe
        float dFirstX = rawX - firstRawX;

        // Get the vertical length of the swipe
        float dFirstY = rawY - firstRawY;

        long touchTime = System.currentTimeMillis() - actionDownTime;

        switch (event.getAction()) {

            // When user's finger touches the screen
            case MotionEvent.ACTION_DOWN:

                firstY = y;

                // Start counting time
                actionDownTime = System.currentTimeMillis();

                setNull_roundedRectanglesWrapperToInterceptEvents();

                // Set starting x
                firstRawX = rawX;

                // Set starting y
                firstRawY = rawY;

                // Set first cursor-moving touch position
                previousCursorMovingTouchPosition = rawX;

                if (inPositionControlMode) {

                    setTrue_roundedRectanglesWrapperToInterceptEvents();

//                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) this.getLayoutParams();
//                    mlp.topMargin = 1000;
//                    this.setLayoutParams(mlp);

                    maxX = parent.getWidth() - this.getWidth();

                    System.out.println("maxX1" + maxX);

//                    maxY = parent.getHeight() - this.getHeight() - (mOnKiboActionListener.getEmojiGroupViewBottomHeight() * 1.05f);
                    maxY = parent.getHeight() - this.getHeight() - kibo_wrapper_view_bottom_padding;

                    firstX_viewPosition_parentRelative = this.getX();
                    firstY_viewPosition_parentRelative = this.getY();

                    SharedPreferences sharedPref = mOnKiboActionListener.getSharedPreferences();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    String orientation = mOnKiboActionListener.getScreenOrientation();
                    editor.putFloat("firstX_viewPosition_parentRelative_" + orientation, firstX_viewPosition_parentRelative);
                    editor.putFloat("firstY_viewPosition_parentRelative_" + orientation, firstY_viewPosition_parentRelative);
                    editor.commit();


                    mOnKiboActionListener.clearSuggestionViews();

                } else {
                    //First two rows should not allow horizontal swiping unless in Emoji tab
                    int viewHeight = Math.round(kibo_height);
                    float firstRowHeight = viewHeight / 4;
                    float secondRowHeight = viewHeight / 4 * 2;
                    if (//!mOnKiboActionListener.isEmojiTheCurrentTab() &&
                            ((y < firstRowHeight && y > -firstRowHeight) || (y < secondRowHeight && y > -secondRowHeight))) {
                        setFalse_roundedRectanglesWrapperToInterceptEvents();

                    }
                }

                break;

            case MotionEvent.ACTION_MOVE:

                float slopeToDifferentiateHorizontalAndVerticalSwipe = 1;

                //When there is text selected, make it harder to swipe vertically, which will erase all selected characters
                if (mOnKiboActionListener.isAnyTextSelected()) {
                    slopeToDifferentiateHorizontalAndVerticalSwipe = 0.25f;
                }

                if (!inPositionControlMode
                        && roundedRectanglesWrapperToInterceptEvents == null
                        && (dFirstX >= roundedRectanglesWrapperInterceptLimit || dFirstX <= -roundedRectanglesWrapperInterceptLimit)
                        && java.lang.Math.abs(dFirstX) > (slopeToDifferentiateHorizontalAndVerticalSwipe * java.lang.Math.abs(dFirstY))
                        && touchTime > horizontal_swipe_time_limit
//                        && event.getPressure() > 0.51
                        ) {

                    setTrue_roundedRectanglesWrapperToInterceptEvents();

                    RoundedRectangleView.delay = -1;

                    int viewHeight = Math.round(kibo_height);

                    float firstRowHeight = viewHeight / 4;
                    float secondRowHeight = viewHeight / 4 * 2;
                    float thirdRowHeight = viewHeight / 4 * 3;

                    if (firstY < firstRowHeight && firstY > -firstRowHeight) {
                        swipeLockedRow = 0;
                        mOnKiboActionListener.changeColorOfRowToActive(0);
                    } else if (firstY < secondRowHeight && firstY > -secondRowHeight) {
                        swipeLockedRow = 1;
                        mOnKiboActionListener.changeColorOfRowToActive(1);
                    } else if (firstY < thirdRowHeight && firstY > -thirdRowHeight) {
                        swipeLockedRow = 2;
                        mOnKiboActionListener.changeColorOfRowToActive(2);
                    } else {
                        swipeLockedRow = 3;
                        mOnKiboActionListener.changeColorOfRowToActive(3);
                    }

                }

                break;

            case MotionEvent.ACTION_UP:

                if (RoundedRectangleView.inputDemoView != null) RoundedRectangleView.inputDemoView.setVisibility(INVISIBLE);

                //make letterAndCursorMoveRepeatingThread wait
                RoundedRectangleView.delay = -1;

        }

        return inPositionControlMode || ((roundedRectanglesWrapperToInterceptEvents != null) && (roundedRectanglesWrapperToInterceptEvents));

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // Get the current x co-ordinate value of the point the finger touches the screen
        float rawX = event.getRawX();
        // Get the current y co-ordinate value of the point the finger touches the screen
        float rawY = event.getRawY();

        // Get the horizontal length of the swipe
        float dFirstX = rawX - firstRawX;
        // Get the vertical length of the swipe
        float dFirstY = rawY - firstRawY;

        // Get the distance between the previousCursorMovingTouchPosition and current position
        float dForCursorMove = rawX - previousCursorMovingTouchPosition;

        long touchTime = System.currentTimeMillis() - actionDownTime;

        if (inPositionControlMode) {

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:

                    break;

                case MotionEvent.ACTION_UP:

                    RoundedRectangleView.inputDemoView.setVisibility(INVISIBLE);

                    // Check if this is a tap or a swipe
                    if (dFirstX < letter_tapLength_maxThreshold && dFirstX > -letter_tapLength_maxThreshold
                            && dFirstY < letter_tapLength_maxThreshold && dFirstY > -letter_tapLength_maxThreshold
                            && touchTime < tap_time_limit) {

                        // Check if this is the second tap of a double tap
                        if (isTapThePreviousAction
                                && actionDownTime - actionUpTime < doubleTap_time_limit) {

                            RoundedRectanglesWrapperView.inPositionControlMode = false;
                            mOnKiboActionListener.resetAllViewsBackToNonPositionControlMode();

                            isTapThePreviousAction = false;

                        } else {
                            isTapThePreviousAction = true;
                        }

                    }

                    actionUpTime = System.currentTimeMillis();

                    break;

                case MotionEvent.ACTION_MOVE:

                    String orientationString = mOnKiboActionListener.getScreenOrientation();

                    //wrapper_background.animate().x(100.4f);
                    //RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this.getLayoutParams();
                    //lp.setMarginEnd(200);

//                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) this.getLayoutParams();
//                    mlp.rightMargin = previousMarginRight - Math.round(dFirstX);
//                    mlp.bottomMargin = previousMarginBottom - Math.round(dFirstY);
//                    this.setLayoutParams(mlp);

                    SharedPreferences sharedPref = mOnKiboActionListener.getSharedPreferences();

//                    if (firstX_viewPosition_parentRelative == -1 || firstY_viewPosition_parentRelative == -1) {

                        firstX_viewPosition_parentRelative = sharedPref.getFloat("firstX_viewPosition_parentRelative_" + orientationString, -1);

                        float kiboAreaHeight = sharedPref.getFloat("kiboAreaHeight" + orientationString, Math.round(RoundedRectangleView.convertMmToPixel(mOnKiboActionListener.getInitialKiboAreaHeight(), getResources())));
                        firstY_viewPosition_parentRelative = sharedPref.getFloat("firstY_viewPosition_parentRelative_" + orientationString, Math.round(RoundedRectangleView.convertMmToPixel(mOnKiboActionListener.getMaxKiboAreaHeight(), getResources()) - kiboAreaHeight));

//                    }


                    if (firstX_viewPosition_parentRelative == -1 || firstY_viewPosition_parentRelative == -1) {

                        firstX_viewPosition_parentRelative = this.getX();
                        firstY_viewPosition_parentRelative = this.getY();

                    }

                    newX = firstX_viewPosition_parentRelative + dFirstX;
                    newY = firstY_viewPosition_parentRelative + dFirstY;

                    System.out.println("firstX_viewPosition_parentRelative_" + orientationString + firstX_viewPosition_parentRelative);
                    System.out.println("dFirstX_" + orientationString + dFirstX);

                    if (maxX == -1 || maxY == -1) {

                        float maxX_default = parent.getWidth() - this.getWidth();


                        int parentHeight = parent.getHeight();
                        int thisHeight = this.getHeight();
//                        float emojiGroupViewBottomHeight = mOnKiboActionListener.getEmojiGroupViewBottomHeight();
                        float maxY_default = parentHeight - thisHeight - kibo_wrapper_view_bottom_padding;

                        System.out.println("maxX_default" + maxX_default);
                        System.out.println("maxY_default" + maxY_default);

                        maxX = sharedPref.getFloat("maxX_" + orientationString, maxX_default);
                        maxY = sharedPref.getFloat("maxY_" + orientationString, maxY_default);

                    }

                    if (newX < 0) {

                        newX = 0;

                    }
                    else if (newX > maxX) newX = maxX;

                    System.out.println("maxX: " + maxX);

                    if (newY < 0) {
                        newY = 0;
                    }
                    else if (newY > maxY) newY = maxY;


                    System.out.println("newX: " + newX);

                    this.animate().setDuration(0).x(newX).y(newY);

                    mOnKiboActionListener.saveEclipseBarsWrapperViewPositionsInSharedPreferences(orientationString, newX, newY, firstX_viewPosition_parentRelative, firstY_viewPosition_parentRelative, maxX, maxY);

                    break;
            }

            return true;

        } else {
            switch (event.getAction()) {

                // When user's finger leaves the screen
                case MotionEvent.ACTION_UP:

                    RoundedRectangleView.inputDemoView.setVisibility(INVISIBLE);

                    mOnKiboActionListener.changeColorOfAllToInactive();

                    //Pause cursor move repeating thread
                    RoundedRectangleView.delay = -1;

                    RoundedRectangleView.cursorActionForRepeating = "";

                    isTapThePreviousAction = false;

                    actionUpTime = System.currentTimeMillis();

                    //if ((roundedRectanglesWrapperToInterceptEvents != null) && (roundedRectanglesWrapperToInterceptEvents == false)) break;

                    if (swipeLockedRow == 0) {
                        // Detect type of swipe and send appropriate signal
                        // Swipe right
                        if (dFirstX >= roundedRectanglesWrapperInterceptLimit) {
                            mOnKiboActionListener.getToPreviousKeyArrangementOfCurrentTab();
                        }
                        // Swipe left
                        else if (dFirstX <= -roundedRectanglesWrapperInterceptLimit) {
                            mOnKiboActionListener.getToNextKeyArrangementOfCurrentTab();
                        }
                    }

                    swipeLockedRow = -1;

                    roundedRectanglesWrapperToInterceptEvents = null;

                    break;

                case MotionEvent.ACTION_MOVE:

                    if (swipeLockedRow == 0 || swipeLockedRow == 1) break;

                    ExtractedText extractedText = mOnKiboActionListener.getExtractedText(new ExtractedTextRequest(), 0);

                    if (extractedText != null) {
                        int currentTextLength = extractedText.text.length();
                        int offset = extractedText.startOffset;
                        int cursorPosition1 = offset + extractedText.selectionStart;
                        int cursorPosition2 = offset + extractedText.selectionEnd;
                        int cursorLeft, cursorRight;

                        if (cursorPosition1 < cursorPosition2) {
                            cursorLeft = cursorPosition1;
                            cursorRight = cursorPosition2;
                        } else {
                            cursorLeft = cursorPosition2;
                            cursorRight = cursorPosition1;
                        }


//                    if ((lockedSelectionLeftward == lockedSelectionRightward && lockedSelectionLeftward == true) ||
//                            (lockedSelectionLeftward == (cursorLeft==cursorRight) && lockedSelectionLeftward == true)
//                            || (lockedSelectionRightward == (cursorLeft==cursorRight) && lockedSelectionRightward == true)) {
//
//                        Log.i("lockedSelectionLeftward", "" + lockedSelectionLeftward);
//                        Log.i("lockedSelectionRightwar", "" + lockedSelectionRightward);
//                        Log.i("cursorLeft==cursorRight", "" + (cursorLeft==cursorRight));
//
//                    }

                        if (Math.abs(dFirstX) < swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                            //Pause letter/cursorMove repeating thread
                            RoundedRectangleView.delay = -1;

                            if (swipeLockedRow == 2) {

                                if (cursorLeft == cursorRight) {

                                    if (dForCursorMove > moveDistanceToMoveCursor) {

                                        if (cursorRight <= currentTextLength) {

                                            int end = cursorLeft + 1;

                                            mOnKiboActionListener.setSelection(end, end, false, null, null);

                                        }

                                        previousCursorMovingTouchPosition = rawX;

                                    } else if (dForCursorMove < -moveDistanceToMoveCursor) {

                                        if (cursorLeft > 0) {

                                            int start = cursorLeft - 1;

                                            mOnKiboActionListener.setSelection(start, start, true, null, null);

                                        }

                                        previousCursorMovingTouchPosition = rawX;

                                    }
                                } else if (lockedSelectionLeftward) {

                                    if (dForCursorMove > moveDistanceToMoveCursor) {

                                        if (cursorRight < currentTextLength) {

                                            int end = cursorRight + 1;

                                            mOnKiboActionListener.setSelection(cursorLeft, end, null, null, false);

                                        }

                                        previousCursorMovingTouchPosition = rawX;

                                    } else if (dForCursorMove < -moveDistanceToMoveCursor) {

                                        int end = cursorRight - 1;

                                        if (cursorLeft == end) {
                                            lockedSelectionLeftward = false;
                                        }

                                        mOnKiboActionListener.setSelection(cursorLeft, end, null, null, true);
                                        previousCursorMovingTouchPosition = rawX;

                                    }

                                } else if (lockedSelectionRightward) {

                                    if (dForCursorMove > moveDistanceToMoveCursor) {

                                        int start = cursorLeft + 1;

                                        if (start == cursorRight) {
                                            lockedSelectionRightward = false;
                                        }

                                        mOnKiboActionListener.setSelection(start, cursorRight, null, false, null);

                                        previousCursorMovingTouchPosition = rawX;

                                    } else if (dForCursorMove < -moveDistanceToMoveCursor) {

                                        if (cursorLeft > 0) {

                                            int start = cursorLeft - 1;

                                            mOnKiboActionListener.setSelection(start, cursorRight, null, true, null);

                                        }

                                        previousCursorMovingTouchPosition = rawX;

                                    }

                                }
                            } else if (swipeLockedRow == 3) {

                                if (cursorLeft == cursorRight) {

                                    if (dForCursorMove > moveDistanceToMoveCursor) {

                                        int end = cursorRight + 1;

                                        if (end <= currentTextLength) {

                                            mOnKiboActionListener.setSelection(cursorLeft, end, null, null, false);

                                            lockedSelectionRightward = true;
                                            lockedSelectionLeftward = false;

                                        }

                                        previousCursorMovingTouchPosition = rawX;

                                    } else if (dForCursorMove < -moveDistanceToMoveCursor) {

                                        if (cursorLeft > 0) {
                                            int start = cursorLeft - 1;
                                            mOnKiboActionListener.setSelection(start, cursorRight, null, true, null);
                                            lockedSelectionLeftward = true;
                                            lockedSelectionRightward = false;
                                        }

                                        previousCursorMovingTouchPosition = rawX;

                                    }

                                } else if (lockedSelectionLeftward) {

                                    if (dForCursorMove > moveDistanceToMoveCursor) {

                                        int start = cursorLeft + 1;

                                        if (start == cursorRight) {
                                            lockedSelectionLeftward = false;
                                        }

                                        mOnKiboActionListener.setSelection(start, cursorRight, null, false, null);

                                        previousCursorMovingTouchPosition = rawX;

                                    } else if (dForCursorMove < -moveDistanceToMoveCursor) {

                                        if (cursorLeft > 0) {

                                            int start = cursorLeft - 1;

                                            mOnKiboActionListener.setSelection(start, cursorRight, null, true, null);

                                        }

                                        previousCursorMovingTouchPosition = rawX;

                                    }
                                } else if (lockedSelectionRightward) {

                                    if (dForCursorMove > moveDistanceToMoveCursor) {

                                        if (cursorRight < currentTextLength) {

                                            int end = cursorRight + 1;

                                            mOnKiboActionListener.setSelection(cursorLeft, end, null, null, false);

                                        }

                                        previousCursorMovingTouchPosition = rawX;

                                    } else if (dForCursorMove < -moveDistanceToMoveCursor) {

                                        int end = cursorRight - 1;

                                        if (cursorLeft == end) {
                                            lockedSelectionRightward = false;
                                        }

                                        mOnKiboActionListener.setSelection(cursorLeft, end, null, null, true);
                                        previousCursorMovingTouchPosition = rawX;

                                    }

                                }
                            }
                        }


                        //Cursor move repeating section
                        else {

                            //previousCursorMovingTouchPosition = -1;

                            if (swipeLockedRow == 2) {

                                if (cursorLeft == cursorRight) {

                                    if (dFirstX >= swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_SINGLE_CURSOR_RIGHT");

                                    } else if (dFirstX < -swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_SINGLE_CURSOR_LEFT");

                                    }
                                } else if (lockedSelectionLeftward) {

                                    if (dFirstX > swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_RIGHT_CURSOR_RIGHT");

                                    } else if (dFirstX < -swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_RIGHT_CURSOR_LEFT");

                                    }

                                } else if (lockedSelectionRightward) {

                                    if (dFirstX > swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_LEFT_CURSOR_RIGHT");

                                    } else if (dFirstX < -swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_LEFT_CURSOR_LEFT");

                                    }

                                }

                            } else if (swipeLockedRow == 3) {

                                if (cursorLeft == cursorRight) {

                                    if (dFirstX > swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("START_TEXT_SELECTION_RIGHT");

                                    } else if (dFirstX < -swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("START_TEXT_SELECTION_LEFT");

                                    }

                                } else if (lockedSelectionLeftward) {

                                    if (dFirstX > swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_LEFT_CURSOR_RIGHT");

                                    } else if (dFirstX < -swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_LEFT_CURSOR_LEFT");

                                    }
                                } else if (lockedSelectionRightward) {

                                    if (dFirstX > swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_RIGHT_CURSOR_RIGHT");

                                    } else if (dFirstX < -swipeZoneLength_forMaxDelayCursorMoveRepeating) {

                                        RoundedRectangleView.setCursorActionForRepeating("MOVE_RIGHT_CURSOR_LEFT");

                                    }

                                }
                            }


                            //Start letter/cursorMove repeating thread
                            synchronized (letterAndCursorMoveRepeatingThread) {

                                RoundedRectangleView.delay = minDelayForCursorMoveRepeating + Math.round((maxDelayForCursorMoveRepeating - minDelayForCursorMoveRepeating)
                                        * (1 - (Math.min((Math.abs(Math.abs(dFirstX) - swipeZoneLength_forMaxDelayCursorMoveRepeating)
                                        / (swipeZoneLength_forMinDelayCursorMoveRepeating - swipeZoneLength_forMaxDelayCursorMoveRepeating)), 1))));

                                letterAndCursorMoveRepeatingThread.notify();

                            }


                        }

                    }

                    break;
            }
        }

        return ((roundedRectanglesWrapperToInterceptEvents !=null)&&(roundedRectanglesWrapperToInterceptEvents));

    }


    public int getSwipeLockedRow() {
        return swipeLockedRow;
    }


    private void getResourceValuesAndSetup() {

        Resources resources = this.getResources();

        tap_time_limit = resources.getInteger(R.integer.tap_time_limit);

        doubleTap_time_limit = resources.getInteger(R.integer.doubleTap_time_limit);

        swipe_time_limit = resources.getInteger(R.integer.swipe_time_limit);

        horizontal_swipe_time_limit = resources.getInteger(R.integer.horizontal_swipe_time_limit);

        ViewConfiguration vc = ViewConfiguration.get(this.getContext());
        int touchSlop = vc.getScaledTouchSlop();

        int baseTouchSlop = resources.getInteger(R.integer.base_scaledTouchSlop);

        int touchSlopDifference = 0;

        if (baseTouchSlop < touchSlop) touchSlopDifference = touchSlop - baseTouchSlop;

        roundedRectanglesWrapperInterceptLimit = touchSlopDifference + RoundedRectangleView.convertDpToPixel(Float.parseFloat(resources.getString(R.string.eclipseBarsWrapper_interceptLimit)),resources);

        moveDistanceToMoveCursor = touchSlopDifference + RoundedRectangleView.convertDpToPixel(resources.getInteger(R.integer.moveDistanceToMoveCursor), resources);

        letter_tapLength_maxThreshold = touchSlopDifference + Float.parseFloat(resources.getString(R.string.letter_tapLength_maxThreshold));

        maxDelayForCursorMoveRepeating = resources.getInteger(R.integer.maxDelayForCursorMoveRepeating);
        minDelayForCursorMoveRepeating = resources.getInteger(R.integer.minDelayForCursorMoveRepeating);
        swipeZoneLength_forMinDelayCursorMoveRepeating = RoundedRectangleView.convertDpToPixel(resources.getInteger(R.integer.swipeZoneLength_forMinDelayCursorMoveRepeating),resources);
        swipeZoneLength_forMaxDelayCursorMoveRepeating = RoundedRectangleView.convertDpToPixel(resources.getInteger(R.integer.swipeZoneLength_forMaxDelayCursorMoveRepeating),resources);

        kibo_wrapper_view_bottom_padding = resources.getDimension(R.dimen.kibo_wrapper_view_bottom_padding);

        kibo_height = resources.getDimension(R.dimen.kibo_height);

    }

    public RoundedRectanglesWrapperView(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);

        getResourceValuesAndSetup();

    }
    public RoundedRectanglesWrapperView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);

        getResourceValuesAndSetup();

    }
    public RoundedRectanglesWrapperView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);

        getResourceValuesAndSetup();

    }

    protected void setOnKiboActionListener(OnKiboActionListener listener) {
        mOnKiboActionListener = listener;
    }

    public long getActionDownTime() {

        return actionDownTime;

    }

    public void setLockedSelectionRightward(boolean input) {

        lockedSelectionRightward = input;

    }

    public void setLockedSelectionLeftward(boolean input) {

        lockedSelectionLeftward = input;

    }

    public void setTrue_roundedRectanglesWrapperToInterceptEvents() {
        RoundedRectangleView.inputDemoView.setVisibility(INVISIBLE);
        roundedRectanglesWrapperToInterceptEvents = true;
    }

    public void setFalse_roundedRectanglesWrapperToInterceptEvents() {
        roundedRectanglesWrapperToInterceptEvents = false;
    }

    public void setNull_roundedRectanglesWrapperToInterceptEvents() {
        roundedRectanglesWrapperToInterceptEvents = null;
    }

    public Boolean isRoundedRectanglesWrapperToInterceptEvents() {
        return roundedRectanglesWrapperToInterceptEvents;
    }

}