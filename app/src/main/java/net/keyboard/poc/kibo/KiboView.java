package net.keyboard.poc.kibo;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * Created by Duy Dang on 1/22/2016.
 */



public class KiboView extends RelativeLayout {

    // Debugging TAG
    private static final String TAG = "Debugging";

    LayoutInflater mInflater;

    // This is the callback to KiboIME
    OnKiboActionListener mOnKiboActionListener;


    public KiboView(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);

    }
    public KiboView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);

    }
    public KiboView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);

    }

    protected void setOnKiboActionListener(OnKiboActionListener listener) {
        mOnKiboActionListener = listener;
    }

}
