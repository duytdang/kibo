package net.keyboard.poc.kibo;

/**
 * Created by Duy Dang on 6/25/2016.
 */
// This interface is implemented by the ViewGroup instance in order to coordinate the Touch events happening to its children views
public interface TouchCoordinator {

    void setTrue_roundedRectanglesWrapperToInterceptEvents();

    void setFalse_roundedRectanglesWrapperToInterceptEvents();

    void setNull_roundedRectanglesWrapperToInterceptEvents();

    Boolean isRoundedRectanglesWrapperToInterceptEvents();

    long getActionDownTime();

    void setIsTapThePreviousAction(boolean input);

    boolean getIsTapThePreviousAction();

    void setPreviouslyTappedRoundedRectangleView(int[] row_column);

    int[] getPreviouslyTappedRoundedRectangleView();

    void setActionUpTime(long input);

    long getActionUpTime();

    void setLockedSelectionRightward(boolean input);

    void setLockedSelectionLeftward(boolean input);

    int getSwipeLockedRow();

    void setLastTappedButton(String input);

    String getLastTappedButton();
}
