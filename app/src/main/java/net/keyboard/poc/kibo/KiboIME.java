package net.keyboard.poc.kibo;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.inputmethodservice.InputMethodService;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.view.textservice.TextServicesManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

/**
 * Created by Duy Dang on 1/16/2016.
 */
public class KiboIME extends InputMethodService implements OnKiboActionListener {

    TensorFlowInferenceInterface tensorflow;

    WindowManager window;

    RelativeLayout mKiboArea;
    KiboView mKiboView;
    KiboViewBackground mKiboViewBackground;
    RoundedRectanglesWrapperView mEclipseBarsWrapperView;

    LinearLayout suggestionViewsLeftWrapper;
    TextView[] suggestionViewsLeft;
    TextView[] suggestionViewsRight;
    String[] wordAndEmojiPredictions;

    TextView wrapper_background;

    RoundedRectangleView[][] rectangleViews = new RoundedRectangleView[5][4];

    KeyArrangementsTabs englishKeyArrangementsTabs = new KeyArrangementsTabs();

    KeyArrangementsTab previousTab = new KeyArrangementsTab();

    //LettersTab
    HashMap<String,String[]> keyArrangementLowerCase, keyArrangementUpperCase;

    //NumbersTab
    HashMap<String,String[]> keyArrangementNumbersTab;

    //SpecialCharsTab
    HashMap<String,String[]> keyArrangementSpecialCharsTab;

    //EmojisTab
    ArrayList<HashMap<String,String[]>> keyArrangementEmojisTabs;

    HashMap<String,String[]> keyIndependentArrangement;

    InputMethodManager mInputMethodManager;
    InputConnection currentInputConnection;
    TextServicesManager tsm;

    HashMap<Character,Character> letterMap;

    float elevation;

//    TextView emojiGroupTop0, emojiGroupTop1, emojiGroupTop2, emojiGroupTop3, emojiGroupTop4, emojiGroupTop5;
    TextView emojiGroupBottom0, emojiGroupBottom1, emojiGroupBottom2, emojiGroupBottom3, emojiGroupBottom4, emojiGroupBottom5;
    TextView nonpredictWordBottom;
    TextView nonpredictWordTop;

    // Temporary uppercase
    private boolean tempUpperCase = false;

    private boolean permanentUpperCase = false;


    int kibo_background_color;
    int inactive_key_background_color;
    int inactive_key_text_color;
    int active_key_background_color;
    int active_key_text_color;


    boolean showLeftSuggestions = true;
    boolean showTopEmojiGroup = false;

    LinearLayout emojiGroup_nonpredict_ViewBottom;

    float wrapperX, wrapperY;

    JSONObject dictionary = null;
    JSONObject inverse_dictionary = null;
    JSONObject unicode_to_emoji = null;


    Integer screenWidth = 1000, screenHeight = 2000, currentAreaWidth = 1000;

    static ArrayList<String> leftWordsList;
    static float[] tensorflowScores;
    static float[] keyStrokeMapScores;
    static float[] wordLengthMatchScores;
    static Integer[] sortedWordsArrayForTransform;
    static Integer[] sortedWordsArrayForSuggestion;
    static Integer[] sortedEmojisArrayForSuggestion;
    static int emojiSize = 107;
    static int tensorflowInputSize = 2;

    float maxKiboAreaHeight = 45.00f;

    float kiboAreaHeight = -1f;

    float initialKiboAreaHeight;

    float kibo_width;

    float kibo_right_margin_initial;

    @Override
    public float getMaxKiboAreaHeight() {
        return maxKiboAreaHeight;
    }

    @Override
    public float getInitialKiboAreaHeight() {
        return initialKiboAreaHeight;
    }

    @Override
    public float getEmojiGroupViewBottomHeight() {
        return emojiGroup_nonpredict_ViewBottom.getHeight();
    }

    @Override
    public void triggerEditorAction() {

        EditorInfo info = getCurrentInputEditorInfo();
        currentInputConnection.performEditorAction(info.actionId);
//        nonpredictWordTop.setText("");
        nonpredictWordBottom.setText("");
        getTensorflowScores("");
        refreshAndResetWordPredViewsBackToNonPosContrlMode();

    }

    @Override
    public boolean isLettersTheCurrentTab() {
        return englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.lettersTab;
    }

    //Implements KiboView.OnKiboActionListener
    @Override
    public void signal_nonpredict(String hashMapKey) {

        String symbol;

        String[] hashMapValue = englishKeyArrangementsTabs.currentTab.currentKeyArrangement.get(hashMapKey);
        if (hashMapValue == null) hashMapValue = englishKeyArrangementsTabs.currentKeyIndependentArrangement.get(hashMapKey);
        if (hashMapValue == null) symbol = hashMapKey;
        else symbol = hashMapValue[0];
        if (!symbol.contains("SpecialAction-")) commitText_toNonpredictView(symbol, 1);

    }

    //Implements KiboView.OnKiboActionListener
    @Override
    public void signal(String hashMapKey) {

        String symbol;

        String[] hashMapValue = englishKeyArrangementsTabs.currentTab.currentKeyArrangement.get(hashMapKey);
        if (hashMapValue == null) hashMapValue = englishKeyArrangementsTabs.currentKeyIndependentArrangement.get(hashMapKey);
        if (hashMapValue == null) symbol = hashMapKey;
        else symbol = hashMapValue[0];

        //If tempUpperCase last time -> any action will lead to a change back to lower case
        if (!symbol.equals("SpecialAction-▲_") && englishKeyArrangementsTabs.lettersTab.currentIndex == 1) {
            if (tempUpperCase) {
                englishKeyArrangementsTabs.lettersTab.setCurrentKeyArrangement(0);
                tempUpperCase = false;
                refreshDisplayCharacters();
            }
        } else {
            tempUpperCase = false;
        }

        if (!symbol.contains("SpecialAction-")) {
            if (symbol.length()>2) commitText(symbol.substring(0,1), 1);
            else commitText(symbol, 1);
        }

        else {

            if (symbol.equals("SpecialAction-⌫")) backspace();

            else if (symbol.equals("SpecialAction-")) blankspace();

            else if (symbol.equals("SpecialAction-▲_") || symbol.equals("SpecialAction-abc")) {

                if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.lettersTab) {

                    permanentUpperCase = false;

                    if (englishKeyArrangementsTabs.lettersTab.currentIndex == 1) {
                        englishKeyArrangementsTabs.lettersTab.setCurrentKeyArrangement(0);
                        tempUpperCase = false;
                    } else {
                        englishKeyArrangementsTabs.lettersTab.setCurrentKeyArrangement(1);
                        tempUpperCase = true;
                    }
                }

                englishKeyArrangementsTabs.setLettersTab();
                refreshDisplayCharacters();
                resetEmojiGroupViewsBackToNonPositionControlMode();

            } else if (symbol.equals("SpecialAction-▲_-Permanent")) {

                if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.lettersTab) {

                    permanentUpperCase = true;

                    englishKeyArrangementsTabs.lettersTab.setCurrentKeyArrangement(1);

                    refreshDisplayCharacters();

                }

            } else if (symbol.equals("SpecialAction-\uD83D\uDC22")) {

                resetAllViewsBackToNonPositionControlMode();

                RoundedRectanglesWrapperView.inPositionControlMode = true;

                RoundedRectangleView.inputDemoView.setVisibility(View.INVISIBLE);

                for (int i = 0; i<4; i++) {
                    suggestionViewsLeft[i].setVisibility(View.INVISIBLE);
                    suggestionViewsRight[i].setVisibility(View.INVISIBLE);
                }

                String orientationString = getScreenOrientation();

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
                kiboAreaHeight = sharedPref.getFloat("kiboAreaHeight" + orientationString, initialKiboAreaHeight);

//                nonpredictWordTop.setVisibility(View.GONE);
                nonpredictWordBottom.setVisibility(View.GONE);
                mEclipseBarsWrapperView.setVisibility(View.INVISIBLE);

                mKiboArea.getLayoutParams().height = Math.round(RoundedRectangleView.convertMmToPixel(maxKiboAreaHeight, getResources()));
                mKiboView.getLayoutParams().height = Math.round(RoundedRectangleView.convertMmToPixel(maxKiboAreaHeight, getResources()));
                mKiboArea.requestLayout();

                mEclipseBarsWrapperView.animate().setDuration(0).y(Math.round(RoundedRectangleView.convertMmToPixel(maxKiboAreaHeight, getResources()) - kiboAreaHeight));

                wrapper_background.bringToFront();
                wrapper_background.setText("Drag to move\nDouble tap to exit");
                mEclipseBarsWrapperView.setVisibility(View.VISIBLE);




            } else if (symbol.equals("SpecialAction-123")) {

                englishKeyArrangementsTabs.setNumbersTab();
                refreshDisplayCharacters();
                resetEmojiGroupViewsBackToNonPositionControlMode();

            } else if (symbol.equals("SpecialAction-#+=")) {

                englishKeyArrangementsTabs.setSpecialCharactersTab();
                refreshDisplayCharacters();
                resetEmojiGroupViewsBackToNonPositionControlMode();

            } else if (symbol.equals("SpecialAction-☺") || symbol.equals("SpecialAction-\uD83D\uDE95")) {

                RoundedRectangleView.inputDemoView.setVisibility(View.INVISIBLE);

                previousTab = englishKeyArrangementsTabs.currentTab;
                englishKeyArrangementsTabs.setEmojisTab();
                refreshDisplayCharacters();
                resetEmojiGroupViewsBackToNonPositionControlMode();

            } else if (symbol.equals("SpecialAction-GO")) {
                triggerEditorAction();
            } else if (symbol.equals("SpecialAction-Prev")) {
                englishKeyArrangementsTabs.emojisTab.getToPreviousKeyArrangement();
                refreshDisplayCharacters();
                resetEmojiGroupViewsBackToNonPositionControlMode();
            } else if (symbol.equals("SpecialAction-Next")) {
                englishKeyArrangementsTabs.emojisTab.getToNextKeyArrangement();
                refreshDisplayCharacters();
                resetEmojiGroupViewsBackToNonPositionControlMode();
            }
        }

    }

    @Override
    public void invertKiboPositionVertically() {

        String orientationString = getScreenOrientation();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        wrapperX = sharedPref.getFloat("wrapperX_" + orientationString, screenWidth - kibo_width - kibo_right_margin_initial);

        float newWrapperX = (currentAreaWidth - wrapperX - mEclipseBarsWrapperView.getWidth());

        mEclipseBarsWrapperView.animate().setDuration(1000).x(Math.round(newWrapperX));

        wrapperX = newWrapperX;

        editor.putFloat("wrapperX_" + orientationString, wrapperX);
        editor.putFloat("firstX_viewPosition_parentRelative_" + orientationString, wrapperX);
        editor.commit();

        resetAllViewsBackToNonPositionControlMode();

    }

    @Override
    public boolean isAnyTextSelected() {
        CharSequence textSelected = currentInputConnection.getSelectedText(0);
        return (textSelected != null && textSelected.length() != 0);
    }

    //Implements KiboView.OnKiboActionListener
    @Override
    public boolean setSelection(int start, int end, Boolean singleCursorLeftward, Boolean leftCursorLeftward, Boolean rightCursorLeftWard) {

//        nonpredictWordTop.setText("");
        nonpredictWordBottom.setText("");

        int newStart = start;
        int newEnd = end;

        boolean a;

        if (singleCursorLeftward != null) {
            CharSequence textBefore = currentInputConnection.getTextBeforeCursor(2, 0);
            CharSequence textAfter = currentInputConnection.getTextAfterCursor(2, 0);

            if (singleCursorLeftward && textBefore != null && textBefore.length() == 2 && Character.isSurrogatePair(textBefore.charAt(0), textBefore.charAt(1))) {
                newStart = newStart - 1;
                a = currentInputConnection.setSelection(newStart, newStart);
            }

            else if (textAfter != null && textAfter.length() == 2 && Character.isSurrogatePair(textAfter.charAt(0), textAfter.charAt(1))) {
                newEnd = newEnd + 1;
                a = currentInputConnection.setSelection(newEnd, newEnd);
            }

            else a = currentInputConnection.setSelection(newEnd, newEnd);

        }

        else {

            if (start != end) {

                CharSequence textSelected = currentInputConnection.getSelectedText(0);
                CharSequence textBefore = currentInputConnection.getTextBeforeCursor(2, 0);
                CharSequence textAfter = currentInputConnection.getTextAfterCursor(2, 0);

                if (leftCursorLeftward != null) {
                    if (leftCursorLeftward) {

                        if (textBefore != null && textBefore.length() == 2 && Character.isSurrogatePair(textBefore.charAt(0), textBefore.charAt(1))) {
                            newStart = newStart - 1;
                        }

                    } else {

                        if (textSelected != null && textSelected.length() > 1 && Character.isSurrogatePair(textSelected.charAt(0), textSelected.charAt(1))) {
                            newStart = newStart + 1;
                        }

                    }
                } else {

                    if (rightCursorLeftWard != null) {
                        if (rightCursorLeftWard) {

                            if (textSelected != null) {

                                int selectedLength = textSelected.length();

                                if (selectedLength > 1 && Character.isSurrogatePair(textSelected.charAt(selectedLength - 2), textSelected.charAt(selectedLength - 1))) {
                                    newEnd = newEnd - 1;
                                }

                            }

                        } else {

                            if (textAfter != null && textAfter.length() == 2 && Character.isSurrogatePair(textAfter.charAt(0), textAfter.charAt(1))) {
                                newEnd = newEnd + 1;
                            }

                        }
                    }

                }

                a = currentInputConnection.setSelection(newStart, newEnd);

            } else {

                a = currentInputConnection.setSelection(start, end);

            }
        }

        long timePointA = System.currentTimeMillis();

        getTensorflowScores("");

        long timePointB = System.currentTimeMillis();

        refreshAndResetWordPredViewsBackToNonPosContrlMode();

        long timePointC = System.currentTimeMillis();

        System.out.println("timePointB - timePointA" + (timePointB - timePointA));
        System.out.println("timePointC - timePointB" + (timePointC - timePointB));

        refreshDisplayNonpredictViews();

        return a;
    }

    //Delete 1 character to the left
    private void backspace() {

        if (currentInputConnection.getSelectedText(0) == null) {

            CharSequence textBefore = currentInputConnection.getTextBeforeCursor(2, 0);

            //Check if this character is half of an emoji
            if (textBefore.length() == 2 && Character.isSurrogatePair(textBefore.charAt(0), textBefore.charAt(1))) {
                    currentInputConnection.deleteSurroundingText(2, 0);
            }
            else currentInputConnection.deleteSurroundingText(1, 0);
        }

        currentInputConnection.commitText("", 0);


        getSurroundingText("");
        getTensorflowScores("");
        getKeyStrokeMapAndLengthMatchScores("");
        predict("");
        transform(0);
        refreshAndResetWordPredViewsBackToNonPosContrlMode();




        String currentNonpredictWord = nonpredictWordBottom.getText().toString();
        if (currentNonpredictWord.length()>0) {
//            nonpredictWordTop.setText(currentNonpredictWord.substring(0, currentNonpredictWord.length()-1));
            nonpredictWordBottom.setText(currentNonpredictWord.substring(0, currentNonpredictWord.length()-1));
        }

        refreshDisplayNonpredictViews();
    }

    //Insert 1 blankspace
    private void blankspace() {

        commitText(" ", 1);

//        nonpredictWordTop.setText("");
        nonpredictWordBottom.setText("");

        refreshDisplayNonpredictViews();

    }

    private void refreshDisplayNonpredictViews() {

        String currentNonpredictWord = nonpredictWordBottom.getText().toString();
        if (!(currentNonpredictWord.length()>0)) {
//            nonpredictWordTop.setVisibility(View.GONE);
            nonpredictWordBottom.setVisibility(View.GONE);
        } else {
            if (isLettersTheCurrentTab()) {
//                if (showTopEmojiGroup) nonpredictWordTop.setVisibility(View.VISIBLE);
//                else
                    nonpredictWordBottom.setVisibility(View.VISIBLE);
            }
        }

    }

    private void predict(String toCommit) {

        System.out.println("predict");

        //Predict for transform
        ArrayList<float[]> scoresInOrderOfImportance = new ArrayList<>();
        scoresInOrderOfImportance.add(keyStrokeMapScores);
        scoresInOrderOfImportance.add(wordLengthMatchScores);
        scoresInOrderOfImportance.add(tensorflowScores);
        sortedWordsArrayForTransform = sortArrayBasedOnScores(0, tensorflowScores.length - emojiSize, scoresInOrderOfImportance);

        //Predict
        scoresInOrderOfImportance = new ArrayList<>();
        scoresInOrderOfImportance.add(keyStrokeMapScores);
//        scoresInOrderOfImportance.add(wordLengthMatchScores);
        scoresInOrderOfImportance.add(tensorflowScores);
        sortedWordsArrayForSuggestion = sortArrayBasedOnScores(0, tensorflowScores.length - emojiSize, scoresInOrderOfImportance);
        sortedEmojisArrayForSuggestion = sortArrayBasedOnScores(tensorflowScores.length - emojiSize, tensorflowScores.length, scoresInOrderOfImportance);

    }

    private Integer[] sortArrayBasedOnScores(int startIndex, int endIndex, final ArrayList<float[]> scoresInOrderOfImportance) {

        class wordComparator implements Comparator<Integer> {
            public int compare(Integer a, Integer b) {
                for (int i=0; i<scoresInOrderOfImportance.size(); i++) {
                    float scoreA = scoresInOrderOfImportance.get(i)[a];
                    float scoreB = scoresInOrderOfImportance.get(i)[b];
                    if (scoreA > scoreB) return -1;
                    if (scoreA < scoreB) return 1;
                }
                return 0;
            }
        }

        int numberOfWords = endIndex - startIndex;
        Integer[] sortedWordArray = new Integer[numberOfWords];
        for (int i=0; i<numberOfWords; i++) {
            sortedWordArray[i] = startIndex + i;
        }

        Arrays.sort(sortedWordArray, new wordComparator());

        return sortedWordArray;
    }


    private void transform(int toAdd) {

        if (leftWordsList.size() == 0) return;

        String current = leftWordsList.get(leftWordsList.size()-1);
        String topPredict = "";
        try {
            topPredict = (String) inverse_dictionary.get(Integer.toString(sortedWordsArrayForTransform[0]));
            if (topPredict.length()<current.length()) return;
            char[] topPredictCharArray = new char[current.length()];
            for (int i=0; i<current.length(); i++) {
                topPredictCharArray[i] = Character.isLetter(current.charAt(i)) && Character.isUpperCase(current.charAt(i)) ?
                        Character.toUpperCase(topPredict.charAt(i)):
                        Character.toLowerCase(topPredict.charAt(i));
            }
            topPredict = String.valueOf(topPredictCharArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (current.substring(0, current.length() - 1).equals(topPredict.substring(0, current.length() - 1))) {
            if (toAdd != 0) {

                currentInputConnection.commitText(topPredict.substring(current.length()-1,current.length()), 1);
            }
        } else {
            currentInputConnection.deleteSurroundingText(current.length() - toAdd, 0);
            currentInputConnection.commitText(topPredict.substring(0, current.length()), current.length());
        }

    }


    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {

            InputStream is = getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    private void getSurroundingText(String toCommit) {
        leftWordsList = new ArrayList<>();

        CharSequence textBefore = currentInputConnection.getTextBeforeCursor(50, 0) + toCommit;

        char tempChar;

        StringBuilder sb = new StringBuilder();
        int leftRegionLength = textBefore.length();

//        if (textAfter != null) rightRegionLength = textAfter.length();
//        if (textSelected != null) selectedRegionLength = textSelected.length();

        for (int i = 0; i < leftRegionLength; i++) {
            tempChar = textBefore.charAt(i);
            if (Character.isLetter(tempChar)) {
                sb.append(tempChar);
            } else if (tempChar=="'".charAt(0)) {
                leftWordsList.add(sb.toString());
                sb = new StringBuilder();
                sb.append(tempChar);
            } else {
                leftWordsList.add(sb.toString());
                sb = new StringBuilder();
            }

            if (i==leftRegionLength-1) {
                if (!Character.isLetter(tempChar) && tempChar!= "'".charAt(0)) {
                    sb = new StringBuilder();
                    sb.append(tempChar);
                }
                leftWordsList.add(sb.toString());
            }
        }
    }

    private void getKeyStrokeMapAndLengthMatchScores(String toCommit) {

        System.out.println("getKeyStrokeMapAndLengthMatchScores");

        keyStrokeMapScores = new float[dictionary.length()];
        wordLengthMatchScores= new float[dictionary.length()];

        String current;
        if (leftWordsList.size() == 0) {
            for (int i=0; i< keyStrokeMapScores.length; i++) {
                keyStrokeMapScores[i] = 0;
                wordLengthMatchScores[i] = 0;
            }
            return;
        } else {
            current = leftWordsList.get(leftWordsList.size()-1);
        }
        if (!Character.isLetter(current.charAt(0)) /*&& current.charAt(0) != "'".charAt(0)*/) {
            for (int i=0; i< keyStrokeMapScores.length; i++) {
                keyStrokeMapScores[i] = 0;
                wordLengthMatchScores[i] = 0;
            }
            return;
        }

        for (int w=0;w<dictionary.length()-emojiSize;w++) {
            String key = null;
            try {
                key = (String) inverse_dictionary.get(Integer.toString(w));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (current.length() > key.length()) continue;
            //Percentage of letters matched in the word in order
            int numberLetterMatchedInOrder = 0;
            for (int i=0; i < current.length(); i++) {
                char char1 = current.charAt(i);
                char char2 = letterMap.get(char1);
                if (Character.toLowerCase(char1) == Character.toLowerCase(key.charAt(i))
                        || Character.toLowerCase(char2) == Character.toLowerCase(key.charAt(i))) {
                    numberLetterMatchedInOrder++;
                }
            }
            float percentLetterMatchedInOrder = ((float) numberLetterMatchedInOrder) / current.length();

            //Percentage of letters matched in the word regardless of order
            int numberLetterMatchedOutOrder = 0;
            HashMap<Character,Object> uniqueLetters = new HashMap<>();
            for (int i=0; i < current.length(); i++) {
                uniqueLetters.put(Character.toLowerCase(key.charAt(i)), null);
            }

            for (int i=0; i < current.length(); i++) {
                char char1 = current.charAt(i);
                char char2 = letterMap.get(char1);
                if (uniqueLetters.containsKey(Character.toLowerCase(char1))
                        || uniqueLetters.containsKey(Character.toLowerCase(char2))) {
                    numberLetterMatchedOutOrder++;
                }
            }
            float percentLetterMatchedOutOrder = ((float) numberLetterMatchedOutOrder) / current.length();

            try {
                keyStrokeMapScores[dictionary.getInt(key)] = (percentLetterMatchedInOrder + percentLetterMatchedOutOrder) / 2;
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //If the current length == key length, reward -> maybe it is the intended word
            if (percentLetterMatchedInOrder == 1 && current.length() == key.length()) {
                try {
                    wordLengthMatchScores[dictionary.getInt(key)] = 1;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    wordLengthMatchScores[dictionary.getInt(key)] = 0;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    private void getTensorflowScores(String toCommit) {

        System.out.println("getTensorflowScores");
        int leftWordsListSize_forTensorflow = leftWordsList.size() - 1;

//        /** One time initialization: */
//        TensorFlowInferenceInterface tensorflow = new TensorFlowInferenceInterface(getAssets(), "file:///android_asset/frozen_model_Hypertest.pb");

        String[] OUTPUT_NODES = {"Hypertest/Model/logits:0"};
//        float[] output = new float[10077 * 4];
        float[] output = new float[10077 * tensorflowInputSize];

//        int[] input = new int[]{firstInput, secondInput, 413, 1234};
//        int[] input = new int[]{secondInput, thirdInput};
        ArrayList<Integer> inputList = new ArrayList<Integer>();

        for (int i = 0; i < leftWordsListSize_forTensorflow; i++) {
            if (!dictionary.has(leftWordsList.get(i))) continue;
            try {
                inputList.add((Integer) dictionary.get(leftWordsList.get(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println("Word " + i + ": " + leftWordsList.get(i));
        }

        int[] input = new int[2];
        // Initialize with 1s
        for (int i = 0; i < tensorflowInputSize; i++) input[i] = 1;

        int sizeDifference = inputList.size() - tensorflowInputSize;

        int outputTimestep;

        if (sizeDifference >= 0) {
            outputTimestep = tensorflowInputSize;
            for (int i = 0; i < tensorflowInputSize; i++) {
                input[tensorflowInputSize - 1 - i] = inputList.get(inputList.size() - 1 - i);
            }
        } else {
            outputTimestep = tensorflowInputSize + sizeDifference;
            for (int i = 0; i < tensorflowInputSize + sizeDifference; i++) {
                input[i] = inputList.get(i);
            }
        }

        long tempTime = System.currentTimeMillis();

//        tensorflow.feed("Hypertest/Placeholder/inputs_placeholder:0", input, 1, 3);
        tensorflow.feed("Hypertest/Placeholder/inputs_placeholder:0", input, 1, 2);

        System.out.println("Tensorflow||Feed time: " + (System.currentTimeMillis() - tempTime));
        tempTime = System.currentTimeMillis();

        tensorflow.run(OUTPUT_NODES);

        System.out.println("Tensorflow||Run time: " + (System.currentTimeMillis() - tempTime));
        tempTime = System.currentTimeMillis();

        tensorflow.fetch("Hypertest/Model/logits:0", output);

        System.out.println("Tensorflow||Fetch time: " + (System.currentTimeMillis() - tempTime));


//        for (int i=9999; i<10020; i++) {
//            System.out.println("" + i + ": " + output[i]);
//        }

        //Just get the output at the needed position based on sizeDifference
        tensorflowScores = new float[dictionary.length()];
        if (outputTimestep > 1) {
            for (int i = 0; i < dictionary.length(); i++) {
                tensorflowScores[i] = output[i + dictionary.length() * (outputTimestep - 1)];
            }
        }


//
//        // loading new input
//        tensorflow.fillNodeFloat("input:0", INPUT_SHAPE, input); // INPUT_SHAPE is an int[] of expected shape, input is a float[] with the input data
//
//        tensorflow.feed("input:0", INPUT_SHAPE, input);
//
//        // running inference for given input and reading output
//        String outputNode = "output:0";
//        String[] outputNodes = {outputNode};
//        tensorflow.runInference(outputNodes);
//        tensorflow.readNodeFloat(outputNode, output); // output is a preallocated float[] in the size of the expected output vector


//        try (Graph g = new Graph()) {
//            final String value = "Hello from " + TensorFlow.version();
//
//            // Construct the computation graph with a single operation, a constant
//            // named "MyConst" with a value "value".
//            try (Tensor t = Tensor.create(value.getBytes("UTF-8"))) {
//                // The Java API doesn't yet include convenience functions for adding operations.
//                g.opBuilder("Const", "MyConst").setAttr("dtype", t.dataType()).setAttr("value", t).build();
//            }
//
//            // Execute the "MyConst" operation in a Session.
//            try (Session s = new Session(g);
//                 Tensor output = s.runner().fetch("MyConst").run().get(0)) {
//                System.out.println(new String(output.bytesValue(), "UTF-8"));
//            }
//        } catch (Exception e) {
//
//        }

    }

    private void commitText_toNonpredictView(String toShow, int number) {
        String tempString = nonpredictWordBottom.getText().toString();
        if ((tempString == null || tempString.isEmpty()) && toShow.length() == 0) {
//            nonpredictWordTop.setVisibility(View.GONE);
            nonpredictWordBottom.setVisibility(View.GONE);
        } else {
            if (showTopEmojiGroup) {
//                nonpredictWordTop.setVisibility(View.VISIBLE);
            } else {
                nonpredictWordBottom.setVisibility(View.VISIBLE);
            }
        }

        ArrayList<String> wordsList = new ArrayList<>();

        if (tempString == null || tempString.isEmpty()) {


            CharSequence textBefore = currentInputConnection.getTextBeforeCursor(50, 0);
//        CharSequence textAfter = currentInputConnection.getTextAfterCursor(50, 0);
//        CharSequence textSelected = currentInputConnection.getSelectedText(0);

            char tempChar;

            StringBuilder sb = new StringBuilder();

            int leftRegionLength = 0;

            if (textBefore != null) {

                leftRegionLength = textBefore.length();

                for (int i = leftRegionLength - 1; i >= 0 ; i--) {

                    tempChar = textBefore.charAt(i);

                    if (Character.isLetter(tempChar)) {
                        sb.append(tempChar);
                        if (i == 0) {
                            sb.reverse();
                            wordsList.add(sb.toString());
                            break;
                        }
                    } else if (tempChar=="'".charAt(0)) {
                        sb.append(tempChar);
                        sb.reverse();
                        wordsList.add(sb.toString());
                        break;
                    } else {
//                if (sb.length()==0) sb.append(" ");

                        sb.reverse();
                        wordsList.add(sb.toString());
                        break;

                    }

                }
            }
        }

        if (wordsList.size() == 0) {
//            nonpredictWordTop.setText(tempString + toShow);
            nonpredictWordBottom.setText(tempString + toShow);
        } else {
//            nonpredictWordTop.setText(wordsList.get(0) + toShow);
            nonpredictWordBottom.setText(wordsList.get(0) + toShow);
        }
    }

    private void commitText(String toCommit, int number) {


        getSurroundingText(toCommit);
        getTensorflowScores(toCommit);
        getKeyStrokeMapAndLengthMatchScores(toCommit);

        //User word frequency dictionary
        //General english word usage frequency
        predict(toCommit);

        if (number > 0 && !Character.isLetter(toCommit.charAt(number-1))) {
            currentInputConnection.commitText(toCommit, number);
        } else {
            String current = leftWordsList.get(leftWordsList.size()-1);
            if (!Character.isLetter(current.charAt(0)) && current.charAt(0)!="'".charAt(0)) {
                currentInputConnection.commitText(toCommit, toCommit.length());
            }
            transform(1);
        }

        refreshAndResetWordPredViewsBackToNonPosContrlMode();
        refreshDisplayNonpredictViews();

    }


    public void saveEclipseBarsWrapperViewPositionsInSharedPreferences(String orientationString, float x, float y, float firstX_viewPosition_parentRelative, float firstY_viewPosition_parentRelative, float maxX, float maxY) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putFloat("wrapperX_" + orientationString, x);
        editor.putFloat("wrapperY_" + orientationString, y);
        editor.putFloat("kiboAreaHeight" + orientationString, RoundedRectangleView.convertMmToPixel(maxKiboAreaHeight, getResources()) - y);

        editor.putFloat("firstX_viewPosition_parentRelative_" + orientationString, firstX_viewPosition_parentRelative);
        editor.putFloat("firstY_viewPosition_parentRelative_" + orientationString, firstY_viewPosition_parentRelative);

        editor.putFloat("maxX_" + orientationString, maxX);
        editor.putFloat("maxY_" + orientationString, maxY);

//        System.out.println("To be saved wrapperX: " + x);

        editor.commit();

    }

    public SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
    }


    public void resetAllViewsBackToNonPositionControlMode() {

        System.out.println("resetAllViewsBackToNonPositionControlMode");
        String orientationString = getScreenOrientation();

        System.out.println("orientationString: " + orientationString);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        Resources resources = this.getResources();

        kiboAreaHeight = sharedPref.getFloat("kiboAreaHeight" + orientationString, initialKiboAreaHeight);

        wrapperX = sharedPref.getFloat("wrapperX_" + orientationString, -1);
//        wrapperY = sharedPref.getFloat("wrapperY_" + orientationString, -1);
        wrapperY = 0;

//        wrapperX = currentAreaWidth - RoundedRectangleView.convertDpToPixel(517, resources);

//        System.out.println("wrapperX1" + wrapperX);

//        if (wrapperX == 0) wrapperX = 200;


        if (kiboAreaHeight != -1) {

            mKiboArea.getLayoutParams().height = (int) kiboAreaHeight;
            mKiboView.getLayoutParams().height = (int) kiboAreaHeight;
            mKiboArea.requestLayout();
            editor.putFloat("kiboAreaHeight_" + orientationString, kiboAreaHeight);

        } else {
            kiboAreaHeight = initialKiboAreaHeight;
            mKiboArea.getLayoutParams().height = (int) kiboAreaHeight;
            mKiboView.getLayoutParams().height = (int) kiboAreaHeight;
            mKiboArea.requestLayout();
            editor.putFloat("kiboAreaHeight_" + orientationString, kiboAreaHeight);
        }



        if (wrapperX != -1) mEclipseBarsWrapperView.animate().setDuration(0).x(wrapperX);
        else {

            wrapperX = currentAreaWidth - kibo_width - kibo_right_margin_initial;

            System.out.println("kibo_width: " + kibo_width);
            System.out.println("currentAreaWidth: " + currentAreaWidth);
            System.out.println("wrapperX_" + orientationString + wrapperX);

            editor.putFloat("wrapperX_" + orientationString, wrapperX);

            mEclipseBarsWrapperView.animate().setDuration(0).x(wrapperX);
        }

        if (wrapperY != -1) mEclipseBarsWrapperView.animate().setDuration(0).y(wrapperY);
        else {

            wrapperY = RoundedRectangleView.convertMmToPixel(0, resources);
            editor.putFloat("wrapperY_" + orientationString, wrapperY);
            mEclipseBarsWrapperView.animate().setDuration(0).y(wrapperY);
        }


//        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mEclipseBarsWrapperView.getLayoutParams();
//        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        lp.setMargins(0,0, Math.round(mKiboView.getWidth() - wrapperX - mEclipseBarsWrapperView.getWidth()),0);

//        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) mEclipseBarsWrapperView.getLayoutParams();
//        mlp.rightMargin = mKiboView.getWidth() - Math.round(wrapperX) - mEclipseBarsWrapperView.getWidth();
//        mEclipseBarsWrapperView.setLayoutParams(mlp);


        resetRRWrapperBoundedViewsBackToNonPositionControlMode();

        getSurroundingText("");
        getTensorflowScores("");
        refreshAndResetWordPredViewsBackToNonPosContrlMode();
        resetEmojiGroupViewsBackToNonPositionControlMode();

    }

    public void repositionEmojiGroupViews() {
        Resources resources = this.getResources();
        //TODO: remove hard coded value
        emojiGroup_nonpredict_ViewBottom.setMinimumHeight(Math.round(RoundedRectangleView.convertMmToPixel(7.90f, resources)));

//        int mEclipseBarsWrapperViewHeight = mEclipseBarsWrapperView.getHeight();
//        if (mEclipseBarsWrapperViewHeight == 0) mEclipseBarsWrapperViewHeight = Math.round(RoundedRectangleView.convertMmToPixel(kiboHeight, resources));

//        emojiGroup_nonpredict_ViewBottom.animate().setDuration(0)
//                .x(wrapperX + RoundedRectangleView.convertDpToPixel(0, resources))
//                .y(wrapperY + mEclipseBarsWrapperViewHeight - RoundedRectangleView.convertMmToPixel(4, resources));

//        emojiGroupViewTop.animate().setDuration(0)
//                .x(wrapperX + RoundedRectangleView.convertDpToPixel(0, resources))
//                .y(wrapperY - RoundedRectangleView.convertMmToPixel(7.90f, resources));



    }

    public void repositionWordPredictionViews() {

        System.out.println("repositionWordPredictionViews");

        Resources resources = this.getResources();

//        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
//
//        wrapperX = sharedPref.getFloat("wrapperX", -1);
//        wrapperY = sharedPref.getFloat("wrapperY", -1);

//        System.out.println("wrapperX: " + wrapperX);

        float suggestionViewHeight = suggestionViewsLeft[0].getHeight();
        if (suggestionViewHeight == 0) suggestionViewHeight = RoundedRectangleView.convertDpToPixel(45, resources);

        int  mEclipseBarsWrapperView_width = mEclipseBarsWrapperView.getWidth();
        if (mEclipseBarsWrapperView_width == 0) mEclipseBarsWrapperView_width = RoundedRectangleView.convertDpToPixel(275, resources);

        System.out.println("suggestionViewHeight: " + suggestionViewHeight);
        System.out.println("mEclipseBarsWrapperView_width: " + mEclipseBarsWrapperView_width);
        System.out.println("wrapperX: " + wrapperX);

//        RelativeLayout.LayoutParams lp;

        suggestionViewsLeftWrapper.animate().setDuration(0)
                .x(wrapperX - RoundedRectangleView.convertDpToPixel(205, resources))
                .y(wrapperY + RoundedRectangleView.convertDpToPixel(4, resources));

        System.out.println("wrapperX - RoundedRectangleView.convertDpToPixel(2 + 200, resources): " + (wrapperX - RoundedRectangleView.convertDpToPixel(2 + 200, resources)));

//        lp = (RelativeLayout.LayoutParams) suggestionViewsLeftWrapper.getLayoutParams();
//        lp.addRule(RelativeLayout.LEFT_OF, R.id.mEclipseBarsWrapperView);


        for (int i = 0; i<4; i++) {

//            lp = (RelativeLayout.LayoutParams) suggestionViewsLeft[i].getLayoutParams();
//            lp.addRule(RelativeLayout.LEFT_OF, R.id.mEclipseBarsWrapperView);


//            suggestionViewsLeft[i].animate().setDuration(0)
//                    .x(wrapperX - RoundedRectangleView.convertDpToPixel(10, resources))
//                    .y(wrapperY + RoundedRectangleView.convertDpToPixel(4, resources) + i * suggestionViewHeight);

            suggestionViewsRight[i].animate().setDuration(0)
                    .x(wrapperX + mEclipseBarsWrapperView_width + RoundedRectangleView.convertDpToPixel( 5, resources))
                    .y(wrapperY + RoundedRectangleView.convertDpToPixel(4, resources) + i * (suggestionViewHeight + RoundedRectangleView.convertDpToPixel(4, resources)));
        }


    }

    public void nonpredictWordClicked(View view) {

        String chosenWord = ((TextView) view).getText().toString();

        ((TextView) view).setText("");

        ExtractedText extractedText = getExtractedText(new ExtractedTextRequest(), 0);

        if (extractedText != null) {

            int offset = extractedText.startOffset;
            int cursorPosition1 = offset + extractedText.selectionStart;
            int cursorPosition2 = offset + extractedText.selectionEnd;

            if (cursorPosition1 == cursorPosition2) {

                CharSequence textBefore = currentInputConnection.getTextBeforeCursor(50, 0);

                char tempChar;

                int leftRegionLength = 0;

                int wordLeftLength = 0;
                int wordRightLength = 0;

                if (textBefore != null) leftRegionLength = textBefore.length();

                for (int i = leftRegionLength - 1; i >= 0 ; i--) {

                    tempChar = textBefore.charAt(i);

                    if (!Character.isLetter(tempChar) && tempChar!="'".charAt(0)) break;

                    wordLeftLength++;

                    if (tempChar=="'".charAt(0)) break;

                }

                setSelection(cursorPosition1 - wordLeftLength, cursorPosition1 + wordRightLength, null, null, null);

            }


            commitText(chosenWord + " ", chosenWord.length() + 1);
//            clearSuggestionViews();

        }
    }

    public void suggestionClicked(View view) {

        String chosenWord = ((TextView) view).getText().toString().trim();

        ExtractedText extractedText = getExtractedText(new ExtractedTextRequest(), 0);

        if (extractedText != null) {

            int offset = extractedText.startOffset;
            int cursorPosition1 = offset + extractedText.selectionStart;
            int cursorPosition2 = offset + extractedText.selectionEnd;

            if (cursorPosition1 == cursorPosition2) {

                CharSequence textBefore = currentInputConnection.getTextBeforeCursor(50, 0);

                char tempChar;

                int leftRegionLength = 0;

                int wordLeftLength = 0;
                int wordRightLength = 0;

                if (textBefore != null) leftRegionLength = textBefore.length();

                for (int i = leftRegionLength - 1; i >= 0 ; i--) {

                    tempChar = textBefore.charAt(i);

                    if (!Character.isLetter(tempChar) && tempChar!="'".charAt(0)) break;

                    wordLeftLength++;

                    if (tempChar=="'".charAt(0)) break;

                }

                setSelection(cursorPosition1 - wordLeftLength, cursorPosition1 + wordRightLength, null, null, null);

            }

            commitText(chosenWord + " ", chosenWord.length() + 1);

        }
    }

    public void refreshAndResetWordPredViewsBackToNonPosContrlMode() {
        System.out.println("refreshAndResetWordPredViewsBackToNonPosContrlMode");

        CharSequence textBefore = currentInputConnection.getTextBeforeCursor(50, 0);
        CharSequence textSelected = currentInputConnection.getSelectedText(0);

        if (!RoundedRectanglesWrapperView.inPositionControlMode && textSelected == null && (textBefore !=null && textBefore.length()>0)) {

            long start = System.currentTimeMillis();

            onGetWordSuggestions();
            showSuggestionViews();

            long difference = System.currentTimeMillis() - start;
            System.out.println("TEST TIME for onGetWordSuggestions: " + difference);

        }
        else clearSuggestionViews();

    }

    public void clearSuggestionViews() {

        for (int i = 0; i < 4; i++) {

//            suggestionViewsLeft[i].setText("");
            wordAndEmojiPredictions[i] = "";
            suggestionViewsLeft[i].setVisibility(View.INVISIBLE);

//            suggestionViewsRight[i].setText("");
            suggestionViewsRight[i].setVisibility(View.INVISIBLE);

        }

    }

    public void onGetWordSuggestions() {

        System.out.println("onGetWordSuggestions");

        try {

            getSurroundingText("");
            int padding = 0;
            String current = leftWordsList.get(leftWordsList.size()-1);
            if (current.equals((String) inverse_dictionary.get(Integer.toString(sortedWordsArrayForSuggestion[0])))) padding = 1;

            for (int i = 0; i < 2; i++) {

                wordAndEmojiPredictions[i] = (String) inverse_dictionary.get(Integer.toString(sortedWordsArrayForSuggestion[i+padding]));
                suggestionViewsLeft[i].setText(" " + wordAndEmojiPredictions[i] + " ");
                suggestionViewsRight[i].setText(" " + wordAndEmojiPredictions[i] + " ");
            }

            for (int i = 0; i < 2; i++) {
                wordAndEmojiPredictions[i + 2] = (String) inverse_dictionary.get(Integer.toString(sortedEmojisArrayForSuggestion[i]));
                suggestionViewsLeft[i + 2].setText(" " + Html.fromHtml("&#x" + wordAndEmojiPredictions[i + 2]) + " ");
                suggestionViewsRight[i + 2].setText(" " + Html.fromHtml("&#x" + wordAndEmojiPredictions[i + 2]) + " ");
            }

        } catch (Throwable ex) {
            System.out.println("EXCEPTION: " + ex.toString());
        }

    }

    @Override public void onCreate() {
        System.out.println("onCreate");
        super.onCreate();


        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);


        tsm = (TextServicesManager) getSystemService(TEXT_SERVICES_MANAGER_SERVICE);


        //Preload letters arrangements into HashMap
        keyArrangementLowerCase = new HashMap<>();
        keyArrangementUpperCase = new HashMap<>();

        keyArrangementNumbersTab = new HashMap<>();

        keyArrangementSpecialCharsTab = new HashMap<>();

        keyArrangementEmojisTabs = new ArrayList<HashMap<String, String[]>>();

        keyIndependentArrangement = new HashMap<>();

        loadCharacterArrangementFromSettings(keyArrangementLowerCase, "_lowercase", "_lowercase");
        loadCharacterArrangementFromSettings(keyArrangementUpperCase, "_uppercase", "_uppercase");
        loadCharacterArrangementFromSettings(keyArrangementNumbersTab, "_numbersTab", "_numbersTab");
        loadCharacterArrangementFromSettings(keyArrangementSpecialCharsTab, "_specialCharsTab", "_specialCharsTab");


        //TODO: 2
        for (int i=0; i<13; i++) {
            HashMap<String, String[]> temp = new HashMap<String, String[]>();
            loadCharacterArrangementFromSettings(temp, "_emojisTab_" + i, "_emojisTab");
            keyArrangementEmojisTabs.add(temp);
        }


        //resource
        Resources resources = this.getResources();
        //Package name
        String packageName = this.getPackageName();
        //Reiterate by column
        String[] tempArray;
        String tempString;

        for (int i=0; i < this.getResources().getInteger(R.integer.independent_count); i++) {
            String hashMapKey = "independent_" + i;
            tempString = resources.getString(resources.getIdentifier(hashMapKey, "string", packageName));
            tempArray = new String[2];
            tempArray[0] = Html.fromHtml(tempString).toString().replaceAll("\\s","");
            tempArray[1] = tempString;
            keyIndependentArrangement.put(hashMapKey, tempArray);
            tempString = resources.getString(resources.getIdentifier(hashMapKey, "string", packageName));
            tempArray = new String[2];
            tempArray[0] = Html.fromHtml(tempString).toString().replaceAll("\\s","");
            tempArray[1] = tempString;
            keyIndependentArrangement.put(hashMapKey, tempArray);
            tempString = resources.getString(resources.getIdentifier(hashMapKey, "string", packageName));
            tempArray = new String[2];
            tempArray[0] = Html.fromHtml(tempString).toString().replaceAll("\\s","");
            tempArray[1] = tempString;
            keyIndependentArrangement.put(hashMapKey, tempArray);

        }

        //Letters tab
        englishKeyArrangementsTabs.lettersTab.keyArrangements.add(keyArrangementLowerCase);
        englishKeyArrangementsTabs.lettersTab.keyArrangements.add(keyArrangementUpperCase);
        englishKeyArrangementsTabs.lettersTab.setCurrentKeyArrangement(0);

        //Numbers tab
        englishKeyArrangementsTabs.numbersTab.keyArrangements.add(keyArrangementNumbersTab);
        englishKeyArrangementsTabs.numbersTab.setCurrentKeyArrangement(0);

        //Special characters tab
        englishKeyArrangementsTabs.specialCharactersTab.keyArrangements.add(keyArrangementSpecialCharsTab);
        englishKeyArrangementsTabs.specialCharactersTab.setCurrentKeyArrangement(0);

        //Emojis tab
        for (int i=0; i<keyArrangementEmojisTabs.size(); i++) {

            englishKeyArrangementsTabs.emojisTab.keyArrangements.add(keyArrangementEmojisTabs.get(i));

        }
        englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(0);

        //Independent
        englishKeyArrangementsTabs.currentKeyIndependentArrangement = keyIndependentArrangement;

        //Get colors
        kibo_background_color = resources.getColor(R.color.kibo_background);
        inactive_key_background_color = resources.getColor(R.color.inactive_key_background);
        inactive_key_text_color = resources.getColor(R.color.inactive_key_text);
        active_key_background_color = resources.getColor(R.color.active_key_background);
        active_key_text_color = resources.getColor(R.color.active_key_text);

    }

    private void loadCharacterArrangementFromSettings(HashMap<String,String[]> keyArrangement, String mainPostFix, String secondaryPostFix) {

        //resource
        Resources resource = this.getResources();
        //Package name
        String packageName = this.getPackageName();
        //Reiterate by column
        String[] tempArray;
        String tempString;
        for (int i=0; i<5; i++) {
            //Reiterate by row
            for (int j=0; j<4; j++) {
                String hashMapKey_swipe_up = "swipe_up_" + "c" + i + "r" + j;
                String hashMapKey_swipe_down = "swipe_down_" + "c" + i + "r" + j;
                String hashMapKey_tap = "tap_" + "c" + i + "r" + j;

                //Swipe
                tempString = resource.getString(resource.getIdentifier(hashMapKey_swipe_up + mainPostFix, "string", packageName));
                tempArray = new String[2];
                tempArray[0] = Html.fromHtml(tempString).toString().replaceAll("\\s","");
                tempArray[1] = tempString.replaceFirst("SpecialAction-","");
                keyArrangement.put(hashMapKey_swipe_up, tempArray);
                tempString = resource.getString(resource.getIdentifier(hashMapKey_swipe_down + mainPostFix, "string", packageName));
                tempArray = new String[2];
                tempArray[0] = Html.fromHtml(tempString).toString().replaceAll("\\s","");
                tempArray[1] = tempString.replaceFirst("SpecialAction-","");
                keyArrangement.put(hashMapKey_swipe_down, tempArray);

                //Tap
                tempString = resource.getString(resource.getIdentifier(hashMapKey_tap + secondaryPostFix, "string", packageName));
                tempArray = new String[2];
                tempArray[0] = Html.fromHtml(tempString).toString().replaceAll("\\s","");
                tempArray[1] = tempString.replaceFirst("SpecialAction-","");
                keyArrangement.put(hashMapKey_tap, tempArray);

            }
        }

    }

    public void getToNextKeyArrangementOfCurrentTab() {
        if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.emojisTab) {
            englishKeyArrangementsTabs.currentTab.getToNextKeyArrangement();
            refreshDisplayCharacters();
            resetEmojiGroupViewsBackToNonPositionControlMode();
        }
    }

    public void getToPreviousKeyArrangementOfCurrentTab() {
        if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.emojisTab) {
            englishKeyArrangementsTabs.currentTab.getToPreviousKeyArrangement();
            refreshDisplayCharacters();
            resetEmojiGroupViewsBackToNonPositionControlMode();
        }
    }

    //TODO: 4
    public void resetEmojisGroup(View view) {

//        if (view == emojiGroupTop0) {
//
//            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(0);
//
//        } else if (view == emojiGroupTop1) {
//
//            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(4);
//
//        } else if (view == emojiGroupTop2) {
//
//            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(7);
//
//        } else if (view == emojiGroupTop3) {
//
//            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(10);
//
//        } else if (view == emojiGroupTop4) {
//
//            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(11);
//
//        } else if (view == emojiGroupTop5) {
//
//            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(12);
//
//        } else

        if (view == emojiGroupBottom0) {

            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(0);

        } else if (view == emojiGroupBottom1) {

            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(4);

        } else if (view == emojiGroupBottom2) {

            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(7);

        } else if (view == emojiGroupBottom3) {

            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(10);

        } else if (view == emojiGroupBottom4) {

            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(11);

        } else if (view == emojiGroupBottom5) {

            englishKeyArrangementsTabs.emojisTab.setCurrentKeyArrangement(12);

        }

        refreshDisplayCharacters();
        resetEmojiGroupViewsBackToNonPositionControlMode();

    }

    public ExtractedText getExtractedText(ExtractedTextRequest req, int number) {

        return currentInputConnection.getExtractedText(req, number);

    }

    @Override
    public void resetRRWrapperBoundedViewsBackToNonPositionControlMode() {

        changeColorOfAllToInactive();

        System.out.println("resetRRWrapperBoundedViewsBackToNonPositionControlMode");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());


        Resources resources = this.getResources();

//        wrapperX = sharedPref.getFloat("wrapperX", mKiboView.getWidth() - mEclipseBarsWrapperView.getWidth() - RoundedRectangleView.convertDpToPixel(4, resources));
//        wrapperY = sharedPref.getFloat("wrapperY", mKiboView.getHeight() - mEclipseBarsWrapperView.getHeight());

        System.out.println("wrapperX:" + wrapperX);

        float currentKiboHorizontalCenterPosition = wrapperX + mEclipseBarsWrapperView.getWidth()/2;

        float parentHorizontalCenterPosition = mKiboView.getWidth()/2;


        this.showLeftSuggestions = currentKiboHorizontalCenterPosition > parentHorizontalCenterPosition;

//        if (mKiboView.getHeight() - wrapperY - mEclipseBarsWrapperView.getHeight() > (rectangleViews[0][0].getHeight()/2.5) + RoundedRectangleView.convertDpToPixel(spaceRequiredForBottomNonpredictView, resources))  this.showTopEmojiGroup = false;
//        else if (mKiboView.getHeight() != 0) this.showTopEmojiGroup = true;

        System.out.println("currentKiboHorizontalCenterPosition: " + currentKiboHorizontalCenterPosition);
        System.out.println("parentHorizontalCenterPosition: " + parentHorizontalCenterPosition);


        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                rectangleViews[j][i].bringToFront();
            }
        }

        wrapper_background.setText("");

        refreshDisplayCharacters();

//        repositionWordPredictionViews();

    }

    private void showSuggestionViews() {

        System.out.println("showSuggestionViews");

        System.out.println("showLeftSuggestions: " + showLeftSuggestions);

        repositionWordPredictionViews();

        if (this.showLeftSuggestions) {
            for (int i = 0; i < 4; i++) {
//                if (!suggestionViewsLeft[i].getText().toString().equals("")) {
                if (!suggestionViewsLeft[i].getText().equals("")) {
//                    if (suggestionViewsLeft[i].getVisibility() != View.VISIBLE)
                    suggestionViewsLeft[i].setVisibility(View.VISIBLE);
                    suggestionViewsRight[i].setVisibility(View.INVISIBLE);
                }
            }
        } else {
            for (int i = 0; i < 4; i++) {
//                if (!suggestionViewsRight[i].getText().toString().equals("")) {
                if (!suggestionViewsRight[i].getText().equals("")) {
//                    if (suggestionViewsRight[i].getVisibility() != View.VISIBLE)
                    suggestionViewsRight[i].setVisibility(View.VISIBLE);
                    suggestionViewsLeft[i].setVisibility(View.INVISIBLE);
                }
            }
        }



//        if (toReset) {
//
//            System.out.println("In toReset");
//
////            mEclipseBarsWrapperView.animate().setDuration(0).x(mEclipseBarsWrapperView.getX() - 1).y(mEclipseBarsWrapperView.getY() - 1);
//
//            RoundedRectanglesWrapperView.inPositionControlMode = true;
//
//            resetAllViewsBackToNonPositionControlMode();
//
//            wrapper_background.bringToFront();
//
//            wrapper_background.setText("Drag to move\nDouble tap to exit");
//
//            RoundedRectanglesWrapperView.inPositionControlMode = false;
//
//            resetAllViewsBackToNonPositionControlMode();
//
//            toReset = false;
//
//        }

    }

    //TODO: future
//    public void resize_larger() {
//
//        ViewGroup.LayoutParams params;
//        for (int i = 0; i < 4; i++) {
//            for (int j = 0; j < 5; j++) {
//                params=rectangleViews[j][i].getLayoutParams();
//                params.width= RoundedRectangleView.convertDpToPixel(96, this.getResources());
//                params.height= RoundedRectangleView.convertDpToPixel(80, this.getResources());
//                rectangleViews[j][i].setLayoutParams(params);
//            }
//        }
//
//    }



    @Override public View onCreateInputView() {
        System.out.println("onCreateInputView");

        RoundedRectangleView.restartDemoPopup = true;

        getResourceValues();

        mKiboArea = (RelativeLayout) getLayoutInflater().inflate(R.layout.input, null);

        mKiboArea.setBackgroundColor(View.INVISIBLE);

        mKiboView = mKiboArea.findViewById(R.id.mKiboView);

        mKiboView.setOnKiboActionListener(this);

        mKiboViewBackground = mKiboView.findViewById(R.id.mKiboViewBackground);

        mKiboViewBackground.setOnKiboActionListener(this);

        mEclipseBarsWrapperView = mKiboView.findViewById(R.id.mEclipseBarsWrapperView);

        suggestionViewsLeft = new TextView[4];
        suggestionViewsRight = new TextView[4];

        wordAndEmojiPredictions = new String[4];
        for (int i=0; i<4; i++) {
            wordAndEmojiPredictions[i] = "";
        }

        suggestionViewsLeftWrapper = (LinearLayout) mKiboView.findViewById(R.id.suggestionLeftWrapper);

        suggestionViewsLeft[0] = (TextView) mKiboView.findViewById(R.id.suggestionLeft1);
        suggestionViewsLeft[1] = (TextView) mKiboView.findViewById(R.id.suggestionLeft2);
        suggestionViewsLeft[2] = (TextView) mKiboView.findViewById(R.id.suggestionLeft3);
        suggestionViewsLeft[3] = (TextView) mKiboView.findViewById(R.id.suggestionLeft4);

        suggestionViewsRight[0] = (TextView) mKiboView.findViewById(R.id.suggestionRight1);
        suggestionViewsRight[1] = (TextView) mKiboView.findViewById(R.id.suggestionRight2);
        suggestionViewsRight[2] = (TextView) mKiboView.findViewById(R.id.suggestionRight3);
        suggestionViewsRight[3] = (TextView) mKiboView.findViewById(R.id.suggestionRight4);

        mEclipseBarsWrapperView.setOnKiboActionListener(this);

        mEclipseBarsWrapperView.parent = mKiboView;

        wrapper_background = (TextView) mKiboView.findViewById(R.id.wrapper_background);

        mEclipseBarsWrapperView.wrapper_background = wrapper_background;

        //position_control = (PositionControlView) mEclipseBarsWrapperView.findViewById(R.id.position_control);

        rectangleViews[0][0] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c0r0);
        rectangleViews[1][0] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c1r0);
        rectangleViews[2][0] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c2r0);
        rectangleViews[3][0] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c3r0);
        rectangleViews[4][0] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c4r0);

        rectangleViews[0][1] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c0r1);
        rectangleViews[1][1] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c1r1);
        rectangleViews[2][1] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c2r1);
        rectangleViews[3][1] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c3r1);
        rectangleViews[4][1] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c4r1);

        rectangleViews[0][2] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c0r2);
        rectangleViews[1][2] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c1r2);
        rectangleViews[2][2] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c2r2);
        rectangleViews[3][2] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c3r2);
        rectangleViews[4][2] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c4r2);

        rectangleViews[0][3] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c0r3);
        rectangleViews[1][3] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c1r3);
        rectangleViews[2][3] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c2r3);
        rectangleViews[3][3] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c3r3);
        rectangleViews[4][3] = (RoundedRectangleView) mEclipseBarsWrapperView.findViewById(R.id.rect_c4r3);

        //TODO: 5
        emojiGroup_nonpredict_ViewBottom = (LinearLayout) mKiboView.findViewById(R.id.emojiGroup_nonpredict_ViewBottom);
        emojiGroupBottom0 = (TextView) mKiboView.findViewById(R.id.emojiGroupBottom0);
        emojiGroupBottom1 = (TextView) mKiboView.findViewById(R.id.emojiGroupBottom1);
        emojiGroupBottom2 = (TextView) mKiboView.findViewById(R.id.emojiGroupBottom2);
        emojiGroupBottom3 = (TextView) mKiboView.findViewById(R.id.emojiGroupBottom3);
        emojiGroupBottom4 = (TextView) mKiboView.findViewById(R.id.emojiGroupBottom4);
        emojiGroupBottom5 = (TextView) mKiboView.findViewById(R.id.emojiGroupBottom5);

//        emojiGroupViewTop = (LinearLayout) mKiboView.findViewById(R.id.emojiGroupViewTop);
//        emojiGroupTop0 = (TextView) mKiboView.findViewById(R.id.emojiGroupTop0);
//        emojiGroupTop1 = (TextView) mKiboView.findViewById(R.id.emojiGroupTop1);
//        emojiGroupTop2 = (TextView) mKiboView.findViewById(R.id.emojiGroupTop2);
//        emojiGroupTop3 = (TextView) mKiboView.findViewById(R.id.emojiGroupTop3);
//        emojiGroupTop4 = (TextView) mKiboView.findViewById(R.id.emojiGroupTop4);
//        emojiGroupTop5 = (TextView) mKiboView.findViewById(R.id.emojiGroupTop5);


        nonpredictWordBottom = (TextView) mKiboView.findViewById(R.id.nonpredictWordBottom);
        nonpredictWordBottom.setText("");

//        nonpredictWordTop = (TextView) mKiboView.findViewById(R.id.nonpredictWordTop);
//        nonpredictWordTop.setText("");

        refreshDisplayNonpredictViews();

        //Apply shadow

//        rect_c0r0.setTranslationZ(elevation);
//        rect_c1r0.setTranslationZ(elevation);
//        rect_c2r0.setTranslationZ(elevation);
//        rect_c3r0.setTranslationZ(elevation);
//
//        rect_c0r1.setTranslationZ(elevation);
//        rect_c1r1.setTranslationZ(elevation);
//        rect_c2r1.setTranslationZ(elevation);
//        rect_c3r1.setTranslationZ(elevation);
//
//        rect_c0r2.setTranslationZ(elevation);
//        rect_c1r2.setTranslationZ(elevation);
//        rect_c2r2.setTranslationZ(elevation);
//        rect_c3r2.setTranslationZ(elevation);

        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                //Assign column to each corresponding eclipse:
                rectangleViews[j][i].setColumnAndRow(j,i);
                //Assign onKiboActionListener
                rectangleViews[j][i].setOnKiboActionListener(this);
                //Assign vibrator
                rectangleViews[j][i].setVibrator(vibrator);
                //Set touchCoordinator
                rectangleViews[j][i].setTouchCoordinator(mEclipseBarsWrapperView);
            }
        }

        RoundedRectangleView.demoView_padding_horizontal = RoundedRectangleView.convertMmToPixel(0.75f,getResources());
        RoundedRectangleView.demoView_padding_vertical = RoundedRectangleView.convertMmToPixel(11.5f, getResources());

        refreshDisplayCharacters();

        return mKiboArea;
    }

    public void resetEmojiGroupViewsBackToNonPositionControlMode() {

        if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.emojisTab){
            showEmojiGroupViews();
        } else {
            hideEmojiGroupViews();
        }

        repositionEmojiGroupViews();
    }

    //TODO: 1
    public void showEmojiGroupViews() {

//        Resources resources = this.getResources();
//        if (mKiboView.getHeight() - wrapperY - mEclipseBarsWrapperView.getHeight() > (rectangleViews[0][0].getHeight()/2.5) + RoundedRectangleView.convertDpToPixel(spaceRequiredForBottomNonpredictView, resources)) showTopEmojiGroup = false;
//        else if (mKiboView.getHeight() != 0) showTopEmojiGroup = true;

        if (showTopEmojiGroup) {
//
//            nonpredictWordTop.setVisibility(View.GONE);
//
//            emojiGroupTop0.setVisibility(View.VISIBLE);
//            emojiGroupTop1.setVisibility(View.VISIBLE);
//            emojiGroupTop2.setVisibility(View.VISIBLE);
//            emojiGroupTop3.setVisibility(View.VISIBLE);
//            emojiGroupTop4.setVisibility(View.VISIBLE);
//            emojiGroupTop5.setVisibility(View.VISIBLE);
//
//
//            if (englishKeyArrangementsTabs.currentTab.currentIndex >= 0
//                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 3) {
//
//                emojiGroupTop0.setText(Html.fromHtml("<big>&#x263A</big>&nbsp&nbsp"));
//                emojiGroupTop1.setText(Html.fromHtml("<small>&#x1F438</small>"));
//                emojiGroupTop2.setText(Html.fromHtml("<small>&#x1F354</small>"));
//                emojiGroupTop3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
//                emojiGroupTop4.setText(Html.fromHtml("<small>&#x1F699</small>"));
//                emojiGroupTop5.setText(Html.fromHtml("<small>&#x26C5</small>"));
//
//            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 4
//                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 6) {
//
//                emojiGroupTop0.setText(Html.fromHtml("<small>&#x263A</small>"));
//                emojiGroupTop1.setText(Html.fromHtml("&nbsp&nbsp<big>&#x1F438</big>&nbsp&nbsp"));
//                emojiGroupTop2.setText(Html.fromHtml("<small>&#x1F354</small>"));
//                emojiGroupTop3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
//                emojiGroupTop4.setText(Html.fromHtml("<small>&#x1F699</small>"));
//                emojiGroupTop5.setText(Html.fromHtml("<small>&#x26C5</small>"));
//
//            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 7
//                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 9) {
//
//                emojiGroupTop0.setText(Html.fromHtml("<small>&#x263A</small>"));
//                emojiGroupTop1.setText(Html.fromHtml("<small>&#x1F438</small>"));
//                emojiGroupTop2.setText(Html.fromHtml("&nbsp&nbsp<big>&#x1F354</big>&nbsp&nbsp"));
//                emojiGroupTop3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
//                emojiGroupTop4.setText(Html.fromHtml("<small>&#x1F699</small>"));
//                emojiGroupTop5.setText(Html.fromHtml("<small>&#x26C5</small>"));
//
//            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 10
//                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 10) {
//
//                emojiGroupTop0.setText(Html.fromHtml("<small>&#x263A</small>"));
//                emojiGroupTop1.setText(Html.fromHtml("<small>&#x1F438</small>"));
//                emojiGroupTop2.setText(Html.fromHtml("<small>&#x1F354</small>"));
//                emojiGroupTop3.setText(Html.fromHtml("&nbsp&nbsp<big>&#x1F3BE</big>&nbsp&nbsp"));
//                emojiGroupTop4.setText(Html.fromHtml("<small>&#x1F699</small>"));
//                emojiGroupTop5.setText(Html.fromHtml("<small>&#x26C5</small>"));
//
//            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 11
//                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 11) {
//
//                emojiGroupTop0.setText(Html.fromHtml("<small>&#x263A</small>"));
//                emojiGroupTop1.setText(Html.fromHtml("<small>&#x1F438</small>"));
//                emojiGroupTop2.setText(Html.fromHtml("<small>&#x1F354</small>"));
//                emojiGroupTop3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
//                emojiGroupTop4.setText(Html.fromHtml("&nbsp&nbsp<big>&#x1F699</big>&nbsp&nbsp"));
//                emojiGroupTop5.setText(Html.fromHtml("<small>&#x26C5</small>"));
//
//            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 12
//                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 12) {
//
//                emojiGroupTop0.setText(Html.fromHtml("<small>&#x263A</small>"));
//                emojiGroupTop1.setText(Html.fromHtml("<small>&#x1F438</small>"));
//                emojiGroupTop2.setText(Html.fromHtml("<small>&#x1F354</small>"));
//                emojiGroupTop3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
//                emojiGroupTop4.setText(Html.fromHtml("<small>&#x1F699</small>"));
//                emojiGroupTop5.setText(Html.fromHtml("&nbsp&nbsp<big>&#x26C5</big>"));
//
//            }

        } else {

            nonpredictWordBottom.setVisibility(View.GONE);

            emojiGroupBottom0.setVisibility(View.VISIBLE);
            emojiGroupBottom1.setVisibility(View.VISIBLE);
            emojiGroupBottom2.setVisibility(View.VISIBLE);
            emojiGroupBottom3.setVisibility(View.VISIBLE);
            emojiGroupBottom4.setVisibility(View.VISIBLE);
            emojiGroupBottom5.setVisibility(View.VISIBLE);


            if (englishKeyArrangementsTabs.currentTab.currentIndex >= 0
                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 3) {

                emojiGroupBottom0.setText(Html.fromHtml("<big>&#x263A</big>&nbsp&nbsp"));
                emojiGroupBottom1.setText(Html.fromHtml("<small>&#x1F438</small>"));
                emojiGroupBottom2.setText(Html.fromHtml("<small>&#x1F354</small>"));
                emojiGroupBottom3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
                emojiGroupBottom4.setText(Html.fromHtml("<small>&#x1F699</small>"));
                emojiGroupBottom5.setText(Html.fromHtml("<small>&#x26C5</small>"));

            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 4
                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 6) {

                emojiGroupBottom0.setText(Html.fromHtml("<small>&#x263A</small>"));
                emojiGroupBottom1.setText(Html.fromHtml("&nbsp&nbsp<big>&#x1F438</big>&nbsp&nbsp"));
                emojiGroupBottom2.setText(Html.fromHtml("<small>&#x1F354</small>"));
                emojiGroupBottom3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
                emojiGroupBottom4.setText(Html.fromHtml("<small>&#x1F699</small>"));
                emojiGroupBottom5.setText(Html.fromHtml("<small>&#x26C5</small>"));

            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 7
                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 9) {

                emojiGroupBottom0.setText(Html.fromHtml("<small>&#x263A</small>"));
                emojiGroupBottom1.setText(Html.fromHtml("<small>&#x1F438</small>"));
                emojiGroupBottom2.setText(Html.fromHtml("&nbsp&nbsp<big>&#x1F354</big>&nbsp&nbsp"));
                emojiGroupBottom3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
                emojiGroupBottom4.setText(Html.fromHtml("<small>&#x1F699</small>"));
                emojiGroupBottom5.setText(Html.fromHtml("<small>&#x26C5</small>"));

            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 10
                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 10) {

                emojiGroupBottom0.setText(Html.fromHtml("<small>&#x263A</small>"));
                emojiGroupBottom1.setText(Html.fromHtml("<small>&#x1F438</small>"));
                emojiGroupBottom2.setText(Html.fromHtml("<small>&#x1F354</small>"));
                emojiGroupBottom3.setText(Html.fromHtml("&nbsp&nbsp<big>&#x1F3BE</big>&nbsp&nbsp"));
                emojiGroupBottom4.setText(Html.fromHtml("<small>&#x1F699</small>"));
                emojiGroupBottom5.setText(Html.fromHtml("<small>&#x26C5</small>"));

            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 11
                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 11) {

                emojiGroupBottom0.setText(Html.fromHtml("<small>&#x263A</small>"));
                emojiGroupBottom1.setText(Html.fromHtml("<small>&#x1F438</small>"));
                emojiGroupBottom2.setText(Html.fromHtml("<small>&#x1F354</small>"));
                emojiGroupBottom3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
                emojiGroupBottom4.setText(Html.fromHtml("&nbsp&nbsp<big>&#x1F699</big>&nbsp&nbsp"));
                emojiGroupBottom5.setText(Html.fromHtml("<small>&#x26C5</small>"));

            } else if (englishKeyArrangementsTabs.currentTab.currentIndex >= 12
                    && englishKeyArrangementsTabs.currentTab.currentIndex <= 12) {

                emojiGroupBottom0.setText(Html.fromHtml("<small>&#x263A</small>"));
                emojiGroupBottom1.setText(Html.fromHtml("<small>&#x1F438</small>"));
                emojiGroupBottom2.setText(Html.fromHtml("<small>&#x1F354</small>"));
                emojiGroupBottom3.setText(Html.fromHtml("<small>&#x1F3BE</small>"));
                emojiGroupBottom4.setText(Html.fromHtml("<small>&#x1F699</small>"));
                emojiGroupBottom5.setText(Html.fromHtml("&nbsp&nbsp<big>&#x26C5</big>"));

            }

        }


    }

    public void hideEmojiGroupViews() {

//        emojiGroupTop0.setVisibility(View.GONE);
//        emojiGroupTop1.setVisibility(View.GONE);
//        emojiGroupTop2.setVisibility(View.GONE);
//        emojiGroupTop3.setVisibility(View.GONE);
//        emojiGroupTop4.setVisibility(View.GONE);
//        emojiGroupTop5.setVisibility(View.GONE);

        emojiGroupBottom0.setVisibility(View.GONE);
        emojiGroupBottom1.setVisibility(View.GONE);
        emojiGroupBottom2.setVisibility(View.GONE);
        emojiGroupBottom3.setVisibility(View.GONE);
        emojiGroupBottom4.setVisibility(View.GONE);
        emojiGroupBottom5.setVisibility(View.GONE);

    }

    public void changeColorOfRowToActive(int row) {

        changeColorOfAllToInactive();

        if (row == 0) {

            for (int i = 0; i < 5; i++) {
                ((GradientDrawable) ((LayerDrawable) rectangleViews[i][0].getBackground()).findDrawableByLayerId(R.id.part1)).setColor(active_key_background_color);
                rectangleViews[i][0].setTextColor(active_key_text_color);
            }

        } else if (row == 1) {

            for (int i = 0; i < 5; i++) {
                ((GradientDrawable) ((LayerDrawable) rectangleViews[i][1].getBackground()).findDrawableByLayerId(R.id.part1)).setColor(active_key_background_color);
                rectangleViews[i][1].setTextColor(active_key_text_color);
            }

        } else if (row == 2) {

            for (int i = 0; i < 5; i++) {
                ((GradientDrawable) ((LayerDrawable) rectangleViews[i][2].getBackground()).findDrawableByLayerId(R.id.part1)).setColor(active_key_background_color);
                rectangleViews[i][2].setTextColor(active_key_text_color);
            }

        } else {
            for (int i = 0; i < 5; i++) {
                ((GradientDrawable) ((LayerDrawable) rectangleViews[i][3].getBackground()).findDrawableByLayerId(R.id.part1)).setColor(active_key_background_color);
                rectangleViews[i][3].setTextColor(active_key_text_color);
            }
        }

    }

    public void changeColorOfAllToInactive() {

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                ((GradientDrawable) ((LayerDrawable) rectangleViews[j][i].getBackground()).findDrawableByLayerId(R.id.part1)).setColor(inactive_key_background_color);
                rectangleViews[j][i].setTextColor(inactive_key_text_color);
            }
        }

    }

    public void refreshDisplayCharacters() {

        HashMap<String,String[]> currentKeyArrangement = englishKeyArrangementsTabs.currentTab.currentKeyArrangement;

//        if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.lettersTab) {


        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {

                if ((i == 2 && (j == 0 || j ==  4)) || (i == 3)) {
                    rectangleViews[j][i].setText(Html.fromHtml(currentKeyArrangement.get("tap_c" + j + "r" + i)[1]));
//                    rectangleViews[j][i].setText(Html.fromHtml("<font color = '#000000'>" + currentKeyArrangement.get("tap_c" + j + "r" + i)[1]
//                            + "</font>"));
                }
                else {
                    if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.emojisTab) {
                        rectangleViews[j][i].setText(Html.fromHtml("<b>" + currentKeyArrangement.get("swipe_up_c" + j + "r" + i)[1] + "</b>"
                                + "<br><font color = '#9097a0'><small><small>" + currentKeyArrangement.get("tap_c" + j + "r" + i)[1]
                                + "</small></small></font><span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>" + "<br>"
                                + "<small>" + currentKeyArrangement.get("swipe_down_c" + j + "r" + i)[1] + "</small>"));
                    }
                    else rectangleViews[j][i].setText(Html.fromHtml(""
//                            + "<small>"
//                            + "<span>&nbsp</span>"
//                            + "</small>"
                            + "<b>" + currentKeyArrangement.get("swipe_up_c" + j + "r" + i)[1] + "</b>"
                            + "<small><small><small><small>"
                            + "<span>&nbsp</span>"
                            + "</small></small></small></small>"
                            + "<small>"
                            + "<sub>"
                            + currentKeyArrangement.get("swipe_down_c" + j + "r" + i)[1]
                            + "</sub>"
                            + "</small>"
                            + "<small>"
//                            + "<span>&nbsp</span>"
                            + "<sup>"
                            + "<font color = '#9097a0'><small><small>" + currentKeyArrangement.get("tap_c" + j + "r" + i)[1]
                            + "</sup>"
                            + "</small>"
                    ));
                }
            }
        }

        if (permanentUpperCase == true && englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.lettersTab) {
            rectangleViews[0][2].setText(Html.fromHtml("<font color = '#000000'><sub>▲</sub></font>" +
                    "<br>" +
                    "<font color = '#000000'><sup><strong>_</strong></sup></font>"));
        }

        //TODO: color
        //d6a017

//            if (permanentUpperCase) {
//
//                rectangleViews[0][0].setText(Html.fromHtml(currentKeyArrangement.get("swipe_up_c0r0")[1] + "<br><font color = '#12964f'><small>"+ "<b>AA</b>" +"</small></font><span>&nbsp&nbsp&nbsp&nbsp&nbsp</span>" + "<br>" + currentKeyArrangement.get("swipe_down_c0r0")[1]));
//
//            } else rectangleViews[0][0].setText(Html.fromHtml(currentKeyArrangement.get("swipe_up_c0r0")[1] + "<br><font color = '#12964f'><small>"+ currentKeyArrangement.get("tap_c0r0")[1]+"</small></font><span>&nbsp&nbsp&nbsp&nbsp&nbsp</span>" + "<br>" + currentKeyArrangement.get("swipe_down_c0r0")[1]));

//        } else {
//
//            for (int i = 0; i < 4; i++) {
//                for (int j = 0; j < 5; j++) {
//                    rectangleViews[j][i].setText(Html.fromHtml("<small>" + currentKeyArrangement.get("swipe_up_c" + j + "r" + i)[1] + "</small>"
//                            + "<br><font color = '#9097a0'><small><small>"+ currentKeyArrangement.get("tap_c" + j + "r" + i)[1]
//                            + "</small></small></font><span>&nbsp&nbsp&nbsp&nbsp&nbsp</span>" + "<br>"
//                            + "&nbsp<small>" + currentKeyArrangement.get("swipe_down_c" + j + "r" + i)[1] + "</small>&nbsp"));
//                }
//            }
//
//            if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.numbersTab) {
//
//                rectangleViews[1][0].setText(Html.fromHtml("<small>" + currentKeyArrangement.get("swipe_up_c1r0")[1] + "</small>"
//                        + "<br><font color = '#d6a017'><small>"+ currentKeyArrangement.get("tap_c1r0")[1]
//                        + "</small></font><span>&nbsp&nbsp&nbsp&nbsp&nbsp</span>" + "<br>"
//                        + "&nbsp<small>" + currentKeyArrangement.get("swipe_down_c1r0")[1] + "</small>&nbsp"));
//
//            } else if (englishKeyArrangementsTabs.currentTab == englishKeyArrangementsTabs.specialCharactersTab) {
//
//                rectangleViews[2][0].setText(Html.fromHtml("<small>" + currentKeyArrangement.get("swipe_up_c2r0")[1] + "</small>"
//                        + "<br><font color = '#c41717'><small>"+ currentKeyArrangement.get("tap_c2r0")[1]
//                        + "</small></font><span>&nbsp&nbsp&nbsp&nbsp&nbsp</span>" + "<br>"
//                        + "&nbsp<small>" + currentKeyArrangement.get("swipe_down_c2r0")[1] + "</small>&nbsp"));
//
//            } else {
//
//                rectangleViews[3][0].setText(Html.fromHtml("<small>" + currentKeyArrangement.get("swipe_up_c3r0")[1] + "</small>"
//                        + "<br><font color = '#d6a017'><small>"+ currentKeyArrangement.get("tap_c3r0")[1]
//                        + "</small></font><span>&nbsp&nbsp&nbsp&nbsp&nbsp</span>" + "<br>"
//                        + "&nbsp<small>" + currentKeyArrangement.get("swipe_down_c3r0")[1] + "</small>&nbsp"));
//
//            }
//        }


        //rect_c3r2.setText(Html.fromHtml("<sub>" + currentKeyArrangement.get("tap_c0r0")[1] + "</sub><span>&nbsp</span>" + "<font color = '#9097a0'><sup><small><small>" + currentKeyArrangement.get("swipe_up_c0r0")[1] + "</small></small></sup></font><span>&nbsp</span>" + "<br>" + currentKeyArrangement.get("swipe_down_c0r0")[1]));
        //rect_c0r0.setText(Html.fromHtml("<span>&nbsp&nbsp&nbsp&nbsp</span>" + keyArrangements.get("swipe_up_c0r0") + "<br>" + "<span>&nbsp&nbsp&nbsp&nbsp</span>" + keyArrangements.get("swipe_down_c0r0") + "<sup>&nbsp&#x0026</sup>"));
        //rect_c0r0.setText("    " + keyArrangements.get("swipe_up_c0r0") + "\n       \u0026\n    " + keyArrangements.get("swipe_down_c0r0"));
        //rect_c0r0.setText(keyArrangements.get("swipe_up_c0r0") + "\n" + keyArrangements.get("swipe_down_c0r0"));

    }

    @Override
    public float getWrapperX() {
        return wrapperX;
    }

    @Override
    public float getWrapperY() {
        return wrapperY;
    }

    @Override
    public boolean bindService(Intent service, ServiceConnection conn, int flags) {
        System.out.println("bindService");
        return super.bindService(service, conn, flags);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        System.out.println("onConfigurationChanged");
    }

    @Override
    public void onBindInput() {
        super.onBindInput();
        System.out.println("onBindInput");
    }


    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        System.out.println("onRebind");
    }


    @Override
    public void onInitializeInterface() {
        super.onInitializeInterface();
        System.out.println("onInitializeInterface");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public ComponentName startService(Intent service) {
        System.out.println("onStartService");
        return super.startService(service);
    }

    @Override public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);

        System.out.println("onStartInput");

    }


    @Override public void onFinishInput() {
        super.onFinishInput();
        System.out.println("onFinishInput");

    }

    @Override public void onFinishInputView(boolean finishingInput){
        super.onFinishInputView(finishingInput);
        System.out.println("onFinishInputView");

//        RoundedRectangleView.restartDemoPopup = true;

        //Need to do this for one eclipse only because letterAndCursorMoveRepeatingThread is a static variable of EclipseBarView class
        rectangleViews[0][0].interruptLetterRepeatingThread();

//        toReset = true;

    }

    @Override public void onStartInputView(EditorInfo attribute, boolean restarting) {
        super.onStartInputView(attribute, restarting);
        System.out.println("onStartInputView");

        currentInputConnection = getCurrentInputConnection();
        //Need to do this for one eclipse only because letterAndCursorMoveRepeatingThread is a static variable of EclipseBarView class
        rectangleViews[0][0].instantiateAndStartLetterRepeatingThread();
        RoundedRectanglesWrapperView.letterAndCursorMoveRepeatingThread = RoundedRectangleView.letterAndCursorMoveRepeatingThread;


        RoundedRectanglesWrapperView.inPositionControlMode = false;

//        nonpredictWordTop.setText("");
        nonpredictWordBottom.setText("");
        refreshDisplayNonpredictViews();

        resetAllViewsBackToNonPositionControlMode();



        String orientationString = getScreenOrientation();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        kiboAreaHeight = sharedPref.getFloat("kiboAreaHeight" + orientationString, initialKiboAreaHeight);
        editor.putFloat("kiboAreaHeight" + orientationString, kiboAreaHeight);

//                nonpredictWordTop.setVisibility(View.GONE);
        nonpredictWordBottom.setVisibility(View.GONE);
        mEclipseBarsWrapperView.setVisibility(View.INVISIBLE);

        mKiboArea.getLayoutParams().height = Math.round(kiboAreaHeight);
        mKiboView.getLayoutParams().height = Math.round(kiboAreaHeight);
        mKiboArea.requestLayout();

        mEclipseBarsWrapperView.animate().setDuration(0).y(0);

        mEclipseBarsWrapperView.setVisibility(View.VISIBLE);




    }

    //The editor reporting movement of its cursor
    @Override public void onUpdateSelection(int oldSelStart, int oldSelEnd,
                                            int newSelStart, int newSelEnd,
                                            int candidatesStart, int candidatesEnd) {
    }

    @Override public void onDisplayCompletions(CompletionInfo[] completions) {
    }

    @Override public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    @Override public String getScreenOrientation() {

        String orientationString = "PORTRAIT";

        Display display = window.getDefaultDisplay();
        int orientation = display.getRotation();

        switch(orientation) {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                orientationString = "PORTRAIT";
                currentAreaWidth = screenWidth;
                break;
            case Surface.ROTATION_90:
            case Surface.ROTATION_270:
                orientationString = "LANDSCAPE";
                currentAreaWidth = screenHeight;
                break;
        }

        return orientationString;
    }

    private void getResourceValues() {

        System.out.println("getResourceValues");

        Resources resources = this.getResources();



        window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = window.getDefaultDisplay();
        Point mPoint = new Point();
        display.getSize(mPoint);
        screenWidth = mPoint.x;
        screenHeight = mPoint.y;

        if (screenWidth > screenHeight) {
            int temp = screenWidth;
            screenWidth = screenHeight;
            screenHeight = temp;
        }

        System.out.println("currentAreaWidth: " + currentAreaWidth);
        System.out.println("screenWidth: " + screenWidth);
        System.out.println("screenHeight: " + screenHeight);


        // Get resource values
        elevation = Float.parseFloat(resources.getString(R.string.elevation));

        letterMap = new HashMap<>();

        letterMap.put('q', 'w');
        letterMap.put('Q', 'W');
        letterMap.put('w', 'q');
        letterMap.put('W', 'Q');

        letterMap.put('e', 'r');
        letterMap.put('E', 'R');
        letterMap.put('r', 'e');
        letterMap.put('R', 'E');

        letterMap.put('t', 'y');
        letterMap.put('T', 'Y');
        letterMap.put('y', 't');
        letterMap.put('Y', 'T');

        letterMap.put('u', 'i');
        letterMap.put('U', 'I');
        letterMap.put('i', 'u');
        letterMap.put('I', 'U');

        letterMap.put('o', 'p');
        letterMap.put('O', 'P');
        letterMap.put('p', 'o');
        letterMap.put('P', 'O');

        letterMap.put('a', 's');
        letterMap.put('A', 'S');
        letterMap.put('s', 'a');
        letterMap.put('S', 'A');

        letterMap.put('d', 'f');
        letterMap.put('D', 'F');
        letterMap.put('f', 'd');
        letterMap.put('F', 'D');

        letterMap.put('g', 'h');
        letterMap.put('G', 'H');
        letterMap.put('h', 'g');
        letterMap.put('H', 'G');

        letterMap.put('j', 'k');
        letterMap.put('J', 'K');
        letterMap.put('k', 'j');
        letterMap.put('K', 'J');

        letterMap.put('l', 'm');
        letterMap.put('L', 'M');
        letterMap.put('m', 'l');
        letterMap.put('M', 'L');

        letterMap.put('z', 'x');
        letterMap.put('Z', 'X');
        letterMap.put('x', 'z');
        letterMap.put('X', 'Z');

        letterMap.put('c', 'v');
        letterMap.put('C', 'V');
        letterMap.put('v', 'c');
        letterMap.put('V', 'C');

        letterMap.put('b', 'n');
        letterMap.put('B', 'N');
        letterMap.put('n', 'b');
        letterMap.put('N', 'B');


        try {

            //For tensorflow
            dictionary = new JSONObject(loadJSONFromAsset("dictionary.json"));
            inverse_dictionary = new JSONObject(loadJSONFromAsset("inverse_dictionary.json"));

            unicode_to_emoji = new JSONObject(loadJSONFromAsset("unicode2emoji.json"));

        } catch (JSONException e) {
            System.out.println(e.getCause());
        }


        tensorflow = new TensorFlowInferenceInterface(getAssets(), "file:///android_asset/frozen_model_Hypertest.pb");


        initialKiboAreaHeight = resources.getDimension(R.dimen.kibo_area_height_initial);
        kibo_width = resources.getDimension(R.dimen.kibo_width);
        kibo_right_margin_initial = resources.getDimension(R.dimen.kibo_right_margin_initial);

    }

}
