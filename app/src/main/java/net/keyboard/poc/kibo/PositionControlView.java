package net.keyboard.poc.kibo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.TextView;

/**
 * Created by Duy Dang on 3/20/2017.
 */

public class PositionControlView extends TextView {

    LayoutInflater mInflater;

    // This variable remembers the x co-ordinate value of the touch ACTION_DOWN event
    private float firstRawX;

    // This variable remembers the y co-ordinate value of the touch ACTION_DOWN event
    private float firstRawY;

    // This is the callback to KiboIME
    OnKiboActionListener mOnKiboActionListener;

    // This is the callback to EclipseBarsWrapperView
    TouchCoordinator mTouchCoordinator;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // This variable gets the current x co-ordinate value (relative to screen) of the point the finger touches the screen
        float rawX = event.getRawX();

        // This variable gets the current y co-ordinate value (relative to screen) of the point the finger touches the screen
        float rawY = event.getRawY();

        // Get the horizontal length of the swipe
        float dFirstX = rawX - firstRawX;

        // Get the vertical length of the swipe
        float dFirstY = rawY - firstRawY;

        //Boolean eclipseBarsWrapperToInterceptEvents = mTouchCoordinator.isRoundedRectanglesWrapperToInterceptEvents();

        switch (event.getAction()) {

            // When user's finger touches the screen
            case MotionEvent.ACTION_DOWN:

                // Set starting x
                firstRawX = rawX;

                // Set starting y
                firstRawY = rawY;

                break;

            // When user's finger leaves the screen
            case MotionEvent.ACTION_UP:




                break;

            case MotionEvent.ACTION_MOVE:

                           //mTouchCoordinator.setFalse_roundedRectanglesWrapperToInterceptEvents();

                            //eclipseBarsWrapperToInterceptEvents = mTouchCoordinator.isRoundedRectanglesWrapperToInterceptEvents();

                break;
        }


        return true;

    }

    public PositionControlView(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);

    }

    public PositionControlView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);
    }

    public PositionControlView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);

    }

}
