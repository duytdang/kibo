package net.keyboard.poc.kibo;

import java.util.HashMap;

/**
 * Created by Dell on 3/4/2017.
 */

public class KeyArrangementsTabs {

    KeyArrangementsTab lettersTab = new KeyArrangementsTab();
    KeyArrangementsTab numbersTab = new KeyArrangementsTab();
    KeyArrangementsTab specialCharactersTab = new KeyArrangementsTab();
    KeyArrangementsTab emojisTab = new KeyArrangementsTab();

    HashMap<String,String[]> currentKeyIndependentArrangement = new HashMap();

    KeyArrangementsTab currentTab = lettersTab;

    public void setLettersTab() {
        currentTab = lettersTab;
    }

    public void setNumbersTab() {
        currentTab = numbersTab;
    }

    public void setSpecialCharactersTab() {
        currentTab = specialCharactersTab;
    }

    public void setEmojisTab() {
        currentTab = emojisTab;
    }

}
