package net.keyboard.poc.kibo;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.RelativeLayout;

/**
 * Created by Duy Dang on 1/22/2016.
 */



public class KiboViewBackground extends RelativeLayout {

    // Debugging TAG
    private static final String TAG = "Debugging";

    LayoutInflater mInflater;

    // This is the callback to KiboIME
    OnKiboActionListener mOnKiboActionListener;


    public KiboViewBackground(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
        getResourceValuesAndSetup();

    }
    public KiboViewBackground(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);
        getResourceValuesAndSetup();

    }
    public KiboViewBackground(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
        getResourceValuesAndSetup();

    }

    protected void setOnKiboActionListener(OnKiboActionListener listener) {
        mOnKiboActionListener = listener;
    }


    float firstRawX;
    float firstRawY;
    float previousCursorMovingTouchPosition;
    long actionDownTime;
    long tap_time_limit;
    long actionUpTime;
    long doubleTap_time_limit;
    float letter_tapLength_maxThreshold = 0.1f;
    boolean isTapThePreviousAction = false;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // Get the current x co-ordinate value of the point the finger touches the screen
        float rawX = event.getRawX();
        // Get the current y co-ordinate value of the point the finger touches the screen
        float rawY = event.getRawY();

        // Get the horizontal length of the swipe
        float dFirstX = rawX - firstRawX;
        // Get the vertical length of the swipe
        float dFirstY = rawY - firstRawY;

        // Get the distance between the previousCursorMovingTouchPosition and current position
        float dForCursorMove = rawX - previousCursorMovingTouchPosition;

        long touchTime = System.currentTimeMillis() - actionDownTime;

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:

                actionDownTime = System.currentTimeMillis();

                firstRawX = rawX;
                firstRawY = rawY;

                break;

            case MotionEvent.ACTION_UP:

                // Check if this is a tap or a swipe
                if (dFirstX < letter_tapLength_maxThreshold && dFirstX > -letter_tapLength_maxThreshold
                        && dFirstY < letter_tapLength_maxThreshold && dFirstY > -letter_tapLength_maxThreshold
                        && touchTime < tap_time_limit) {

                    // Check if this is the second tap of a double tap
                    if (isTapThePreviousAction
                            && actionDownTime - actionUpTime < doubleTap_time_limit) {

                        if (!RoundedRectanglesWrapperView.inPositionControlMode) mOnKiboActionListener.invertKiboPositionVertically();

                        isTapThePreviousAction = false;

                    } else {
                        isTapThePreviousAction = true;
                    }

                }

                actionUpTime = System.currentTimeMillis();

                break;
        }

        return true;
    }

    private void getResourceValuesAndSetup() {

        Resources resources = this.getResources();

        tap_time_limit = resources.getInteger(R.integer.tap_time_limit);

        doubleTap_time_limit = resources.getInteger(R.integer.doubleTap_time_limit);

        ViewConfiguration vc = ViewConfiguration.get(this.getContext());
        int touchSlop = vc.getScaledTouchSlop();

        int baseTouchSlop = resources.getInteger(R.integer.base_scaledTouchSlop);

        int touchSlopDifference = 0;

        if (baseTouchSlop < touchSlop) touchSlopDifference = touchSlop - baseTouchSlop;

        letter_tapLength_maxThreshold = touchSlopDifference + Float.parseFloat(resources.getString(R.string.letter_tapLength_maxThreshold));

    }

}
