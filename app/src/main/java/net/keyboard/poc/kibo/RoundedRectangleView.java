package net.keyboard.poc.kibo;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;


/**
 * Created by Duy Dang on 1/22/2016.
 */
public class RoundedRectangleView extends android.support.v7.widget.AppCompatTextView {

    // Debugging TAG
    private static final String TAG = "Debugging";

    LayoutInflater mInflater;

    Vibrator vibrator;

    // turn on or off vibration
    private boolean vibration_swipe_on = false;
    private boolean vibration_tap_on = false;

    // This is the callback to KiboIME
    OnKiboActionListener mOnKiboActionListener;

    // This is the callback to EclipseBarsWrapperView
    TouchCoordinator mTouchCoordinator;

    // This variable remember if user has swipe up for automatically repeating the letter so that ACTION_UP happens, no more letter will be sent
    private boolean automaticRepeatTriggered = false;

    // This variable states which column this instance of EclipseBarView represents
    private int column;

    // This variable states which row this instance of EclipseBarView represents
    private int row;

    // This variable remembers the x co-ordinate value of the touch ACTION_DOWN event
    private float firstRawX;

    // This variable remembers the y co-ordinate value of the touch ACTION_DOWN event
    private float firstRawY;

    // This variable remembers the x co-ordinate value of the previous touch ACTION_MOVE event
    private float previousRawX;

    // This variable remembers the y co-ordinate value of the previous touch ACTION_MOVE event
    private float previousRawY;

    // This variable helps check if the first letter candidate has not been showed yet
    private boolean candidateShown = false;

    // This variable saves TextView text's default color
    private ColorStateList defaultTextColor;

    // Store the currently chosen character
    static String chosenCharacter = "";
    static String chosenCharacter_nonpredict = "";

    // Store the currently chosen character
    static String currentRoundedRectangleView = "";

    // Store the previously chosen character
    private String previouslyChosenCharacter_nonpredict = "";

    // Height of the eclipse_bar to be achieved from kibo_settings.xml
    private float eclipse_height = 0;


    // The lengths are predefined in kibo_settings.xml

    private float letter_swipeLength_minThreshold = 0.1f;
    private float letter_tapLength_maxThreshold = 0.1f;

    // Tap definition
    private int tap_time_limit;
    private int doubleTap_time_limit;
    private int swipe_time_limit;

    // Vibration
    private int vibration_time = 0;

    // Letter repeating
    static LetterAndCursorMoveRepeatingThread letterAndCursorMoveRepeatingThread;
    static Handler mHandler;
    static volatile int delay = -1;
    static volatile boolean interruptedLetterRepeatingThread = false;
    static boolean long_press_completed = false;
    private boolean letterChoiceRepeatingLocked = false; //lock letter choice when user started using repeating letter feature on a letter
    private int maxDelayForLetterRepeating;
    private int minDelayForLetterRepeating;
    private float swipeZoneLength_forMinDelayLetterRepeating;
    private float swipeZoneLength_forMaxDelayLetterRepeating;

    static boolean restartDemoPopup = false;

    private GradientDrawable gradientDrawable;

//    int inactive_key_background_color = Color.parseColor("#00FFFFFF");
//    int active_key_background_color = Color.parseColor("#000000");


    int kibo_background_color;
    int inactive_key_background_color;
    int inactive_key_text_color;
    int active_key_background_color;
    int active_key_text_color;


    float demoViewWidth, demoInputViewHeight;


    static String cursorActionForRepeating = "";

    static PopupWindow demoViewPopUp;
    static TextView demoView, demoViewComplementary;

    static LayoutInflater inflater;
    static LinearLayout inputDemoView, demoViewLayout;

    static float demoView_padding_horizontal, demoView_padding_vertical;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // This variable gets the current x co-ordinate value (relative to screen) of the point the finger touches the screen
        float rawX = event.getRawX();

        // This variable gets the current y co-ordinate value (relative to screen) of the point the finger touches the screen
        float rawY = event.getRawY();

        // Get the horizontal length of the swipe
        float dFirstX = rawX - firstRawX;

        // Get the vertical length of the swipe
        float dFirstY = rawY - firstRawY;

        Boolean eclipseBarsWrapperToInterceptEvents = mTouchCoordinator.isRoundedRectanglesWrapperToInterceptEvents();

        long touchTime = System.currentTimeMillis() - mTouchCoordinator.getActionDownTime();

        switch (event.getAction()) {

            // When user's finger touches the screen
            case MotionEvent.ACTION_DOWN:

                gradientDrawable.setColor(active_key_background_color);

                showDemoView();

                // Set starting x
                firstRawX = rawX;

                // Set starting y
                firstRawY = rawY;

                // Set previous x
                previousRawX = rawX;

                // Set previous y
                previousRawY = rawY;

                candidateShown = false;
                chosenCharacter = "";
                chosenCharacter_nonpredict = "";
                previouslyChosenCharacter_nonpredict = "";

                //Already done in up but done again here for reliability
                letterChoiceRepeatingLocked = false;

                //Long-press initiated if applicable:
                if (row == 0 && column > 2) {
                    delay = -2;
                    currentRoundedRectangleView = "c" + column + "r" + row;
                    synchronized (letterAndCursorMoveRepeatingThread) {
                        letterAndCursorMoveRepeatingThread.notify();
                    }
                }


                break;

            // When user's finger leaves the screen
            case MotionEvent.ACTION_UP:

                hideDemoView();

                //make letterAndCursorMoveRepeatingThread wait
                delay = -1;


                float currentY = event.getY();

                //Detect if this is a tap-only button
                if (this.row == 3 || (this.row == 2 && (this.column == 0 || this.column == 4))) {

                    //Detect if this is the second tap of a double tap
                    if (mTouchCoordinator.getIsTapThePreviousAction()
                            && mTouchCoordinator.getLastTappedButton().equals("c" + column + "r" + row)
//                            && touchTime < tap_time_limit
                            && mTouchCoordinator.getActionDownTime() - mTouchCoordinator.getActionUpTime() < doubleTap_time_limit) {

                        mOnKiboActionListener.signal("tap_" + "c" + column + "r" + row);

                        if (column == 0 && row == 2) {
                            mOnKiboActionListener.signal("SpecialAction-▲_-Permanent");
                        }

                        mTouchCoordinator.setLastTappedButton("");
                        mTouchCoordinator.setIsTapThePreviousAction(false);

                    } else {

                        mOnKiboActionListener.signal("tap_" + "c" + column + "r" + row);

                        mTouchCoordinator.setIsTapThePreviousAction(true);
                        mTouchCoordinator.setLastTappedButton("c" + column + "r" + row);
                    }


                } else {
                    if (!long_press_completed) {
                        if (chosenCharacter_nonpredict.equals("")) {
                            mOnKiboActionListener.signal_nonpredict("swipe_up_" + "c" + column + "r" + row);
                        } else {
                            mOnKiboActionListener.signal_nonpredict(chosenCharacter_nonpredict);
                        }
                        if (chosenCharacter.equals("")) {
                            mOnKiboActionListener.signal("swipe_up_" + "c" + column + "r" + row);
                        } else {
                            mOnKiboActionListener.signal(chosenCharacter);
                        }
                    }
                }

                gradientDrawable.setColor(inactive_key_background_color);

                long_press_completed = false;
                currentRoundedRectangleView = "";

                letterChoiceRepeatingLocked = false;

                previouslyChosenCharacter_nonpredict = "";

                chosenCharacter = "";
                chosenCharacter_nonpredict = "";

                candidateShown = false;

                mTouchCoordinator.setNull_roundedRectanglesWrapperToInterceptEvents();

                mTouchCoordinator.setActionUpTime(System.currentTimeMillis());

                /*

                // Detect type of swipe and send appropriate signal
                // Swipe up
                if (dFirstY <= -longSwipe) mOnKiboActionListener.signal("swipe_" + column + "0");
                else if (dFirstY > -longSwipe && dFirstY <= -midSwipe) mOnKiboActionListener.signal("swipe_" + column + "1");
                else if (dFirstY > -midSwipe && dFirstY <= -shortSwipe) mOnKiboActionListener.signal("swipe_" + column + "2");
                // Swipe down
                else if (dFirstY > shortSwipe && dFirstY <= midSwipe) mOnKiboActionListener.signal("swipe_" + column + "3");
                else if (dFirstY > midSwipe && dFirstY <= longSwipe) mOnKiboActionListener.signal("swipe_" + column + "4");
                else if (dFirstY > longSwipe) mOnKiboActionListener.signal("swipe_" + column + "5");

                */

                break;

            case MotionEvent.ACTION_MOVE:

                if (!letterChoiceRepeatingLocked) {

//                    if (this.row != 0 || this.column != 4) {

                        if (eclipseBarsWrapperToInterceptEvents == null) {

                            float slopeToDifferentiateVerticalAndHorizontalSwipe = 1;

                            if (this.row == 0 || this.row == 1) {
                                mTouchCoordinator.setFalse_roundedRectanglesWrapperToInterceptEvents();
                                eclipseBarsWrapperToInterceptEvents = mTouchCoordinator.isRoundedRectanglesWrapperToInterceptEvents();
                            }


                        }

                        // Decide the currently chosen character
                        // After just entering a new zone (defined in kibo_settings.xml as letter_swipeZone_length -> slightly swiping to the opposite direction will not go back
                        // to the previously chosen character -> will need to pass a certain range (defined in kibo_settings.xml as letter_swipeZone_gapLength)
                        if (dFirstY <= -letter_swipeLength_minThreshold) {
//                            delay = -1;
//                            chosenCharacter = "swipe_up_" + "c" + column + "r" + row;

                        } else if (dFirstY > letter_swipeLength_minThreshold) {
//                            delay = -1;
                            if (!mOnKiboActionListener.isLettersTheCurrentTab()) chosenCharacter = "swipe_down_" + "c" + column + "r" + row;
                            chosenCharacter_nonpredict = "swipe_down_" + "c" + column + "r" + row;
                        }

                }


                //letter repeating section
                if (eclipseBarsWrapperToInterceptEvents != null) {
                    delay = -2;
                }


                break;
        }


        return true;

    }

    public void showDemoView() {

        if (row == 3 && column == 2) return;

        if (restartDemoPopup && demoViewPopUp!=null) {
            demoViewPopUp.dismiss();
            restartDemoPopup = false;
        }

        if (demoViewPopUp == null || restartDemoPopup) {
            demoViewPopUp = new PopupWindow(getContext());
            demoViewPopUp.setBackgroundDrawable(null);
            demoViewPopUp.setTouchable(false);
            demoViewPopUp.setWidth(Math.round(demoViewWidth));
            demoViewPopUp.setHeight(Math.round(demoInputViewHeight));
            demoViewPopUp.setClippingEnabled(false);
        }
        if (inputDemoView == null) {
            if (inflater == null) inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inputDemoView = (LinearLayout) inflater.inflate(R.layout.input_demo_view, null);
        }

        if (demoViewLayout == null || demoView == null || demoViewComplementary == null) {
            demoViewLayout = inputDemoView.findViewById(R.id.demoViewLayout);
            demoView = demoViewLayout.findViewById(R.id.demoView);
            demoViewComplementary = demoViewLayout.findViewById(R.id.demoViewComplementary);
            demoViewPopUp.setContentView(inputDemoView);
        }


        if (demoViewPopUp.isShowing()) {

            demoViewPopUp.update(Math.round(mOnKiboActionListener.getWrapperX() + getX() - demoView_padding_horizontal),
                    Math.round(mOnKiboActionListener.getWrapperY() + getY() - demoView_padding_vertical),
                    Math.round(demoViewWidth),
                    Math.round(demoInputViewHeight)
                    );

        } else {

            demoViewPopUp.showAtLocation((View) mTouchCoordinator, Gravity.NO_GRAVITY,
                Math.round(mOnKiboActionListener.getWrapperX() + getX() - demoView_padding_horizontal),
                Math.round(mOnKiboActionListener.getWrapperY() + getY() - demoView_padding_vertical));

        }

        demoView.setText(getText());
        inputDemoView.setVisibility(VISIBLE);
        demoViewLayout.setVisibility(VISIBLE);
        demoView.setVisibility(VISIBLE);
        demoViewComplementary.setVisibility(VISIBLE);

    }

    public void hideDemoView() {

        if (row == 3 && column == 2) return;

        inputDemoView.setVisibility(INVISIBLE);

    }

    public void setColumnAndRow(int column, int row) {
        this.column = column;
        this.row = row;
    }

    private void instantiateHandler() {

        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message inputMessage) {
//                if (chosenCharacter != null && chosenCharacter != "") mOnKiboActionListener.signal(chosenCharacter);
                if (delay >= 0) {

                    ExtractedText extractedText = mOnKiboActionListener.getExtractedText(new ExtractedTextRequest(), 0);

                    if (extractedText != null) {

                        int currentTextLength = extractedText.text.length();
                        int offset = extractedText.startOffset;
                        int cursorPosition1 = offset + extractedText.selectionStart;
                        int cursorPosition2 = offset + extractedText.selectionEnd;
                        int cursorLeft, cursorRight;

                        if (cursorPosition1 < cursorPosition2) {
                            cursorLeft = cursorPosition1;
                            cursorRight = cursorPosition2;
                        } else {
                            cursorLeft = cursorPosition2;
                            cursorRight = cursorPosition1;
                        }

                        switch (cursorActionForRepeating) {

                            case "MOVE_SINGLE_CURSOR_RIGHT":

                                if (cursorRight <= currentTextLength) {

                                    int end = cursorLeft + 1;

                                    mOnKiboActionListener.setSelection(end, end, false, null, null);

                                }

                                break;

                            case "MOVE_SINGLE_CURSOR_LEFT":

                                if (cursorLeft > 0) {

                                    int start = cursorLeft - 1;

                                    mOnKiboActionListener.setSelection(start, start, true, null, null);

                                }

                                break;

                            case "MOVE_RIGHT_CURSOR_RIGHT":

                                if (cursorRight < currentTextLength) {

                                    int end = cursorRight + 1;

                                    mOnKiboActionListener.setSelection(cursorLeft, end, null, null, false);

                                }

                                break;

                            case "MOVE_RIGHT_CURSOR_LEFT":

                                int end = cursorRight - 1;

                                if (cursorLeft == end) {

                                    if (mTouchCoordinator.getSwipeLockedRow() == 2) {
                                        mTouchCoordinator.setLockedSelectionLeftward(false);
                                        cursorActionForRepeating = "MOVE_SINGLE_CURSOR_LEFT";

                                    } else if (mTouchCoordinator.getSwipeLockedRow() == 3) {
                                        mTouchCoordinator.setLockedSelectionRightward(false);
                                        cursorActionForRepeating = "START_TEXT_SELECTION_LEFT";
                                    }


                                }

                                mOnKiboActionListener.setSelection(cursorLeft, end, null, null, true);

                                break;

                            case "MOVE_LEFT_CURSOR_RIGHT":

                                int start = cursorLeft + 1;

                                if (start == cursorRight) {

                                    if (mTouchCoordinator.getSwipeLockedRow() == 2) {
                                        mTouchCoordinator.setLockedSelectionRightward(false);
                                        cursorActionForRepeating = "MOVE_SINGLE_CURSOR_RIGHT";
                                    } else if (mTouchCoordinator.getSwipeLockedRow() == 3) {
                                        mTouchCoordinator.setLockedSelectionLeftward(false);
                                        cursorActionForRepeating = "START_TEXT_SELECTION_RIGHT";
                                    }

                                }

                                mOnKiboActionListener.setSelection(start, cursorRight, null, false, null);

                                break;

                            case "MOVE_LEFT_CURSOR_LEFT":

                                if (cursorLeft > 0) {

                                    start = cursorLeft - 1;

                                    mOnKiboActionListener.setSelection(start, cursorRight, null, true, null);

                                }

                                break;

                            case "START_TEXT_SELECTION_RIGHT":

                                end = cursorRight + 1;

                                if (end <= currentTextLength) {

                                    mOnKiboActionListener.setSelection(cursorLeft, end, null, null, false);

                                    mTouchCoordinator.setLockedSelectionRightward(true);

                                }


                                break;

                            case "START_TEXT_SELECTION_LEFT":

                                if (cursorLeft > 0) {
                                    start = cursorLeft - 1;
                                    mOnKiboActionListener.setSelection(start, cursorRight, null, true, null);
                                    mTouchCoordinator.setLockedSelectionLeftward(true);
                                }

                                break;


                        }
                    }

                }
                //Long press case
                else if (delay == -2) {
                    mOnKiboActionListener.signal("tap_" + currentRoundedRectangleView);
                    long_press_completed = true;
                    delay = -1;
                }
            }
        };

    }

    public static void setCursorActionForRepeating(String cursorAction) {

        chosenCharacter = null;
        cursorActionForRepeating = cursorAction;

    }

    public void instantiateAndStartLetterRepeatingThread() {
        if (letterAndCursorMoveRepeatingThread == null) {
            letterAndCursorMoveRepeatingThread = new LetterAndCursorMoveRepeatingThread();
        }

        if (!letterAndCursorMoveRepeatingThread.isAlive()) {
            interruptedLetterRepeatingThread = false;
            letterAndCursorMoveRepeatingThread.start();
        }

    }


    public void interruptLetterRepeatingThread() {

        if (letterAndCursorMoveRepeatingThread.isAlive()) {
            interruptedLetterRepeatingThread = true;
            letterAndCursorMoveRepeatingThread = null;
        }

    }


    private void getResourceValuesAndSetup() {

        Resources resources = this.getResources();

        // Get resource values

        eclipse_height = resources.getDimension(R.dimen.eclipse_height);

        tap_time_limit = resources.getInteger(R.integer.tap_time_limit);
        doubleTap_time_limit = resources.getInteger(R.integer.doubleTap_time_limit);
        swipe_time_limit = resources.getInteger(R.integer.swipe_time_limit);

        vibration_time = resources.getInteger(R.integer.vibration_time);
        vibration_swipe_on = resources.getBoolean(R.bool.vibration_swipe_on);
        vibration_tap_on = resources.getBoolean(R.bool.vibration_tap_on);

        maxDelayForLetterRepeating = resources.getInteger(R.integer.maxDelayForLetterRepeating);
        minDelayForLetterRepeating = resources.getInteger(R.integer.minDelayForLetterRepeating);
        swipeZoneLength_forMinDelayLetterRepeating = convertDpToPixel(resources.getInteger(R.integer.swipeZoneLength_forMinDelayLetterRepeating),resources);
        swipeZoneLength_forMaxDelayLetterRepeating = convertDpToPixel(resources.getInteger(R.integer.swipeZoneLength_forMaxDelayLetterRepeating),resources);

        ViewConfiguration vc = ViewConfiguration.get(this.getContext());
        int touchSlop = vc.getScaledTouchSlop();

        int baseTouchSlop = resources.getInteger(R.integer.base_scaledTouchSlop);

        int touchSlopDifference = 0;

        if (baseTouchSlop < touchSlop) touchSlopDifference = touchSlop - baseTouchSlop;

        letter_swipeLength_minThreshold = touchSlopDifference + Float.parseFloat(resources.getString(R.string.letter_swipeLength_minThreshold));

        letter_tapLength_maxThreshold = touchSlopDifference + Float.parseFloat(resources.getString(R.string.letter_tapLength_maxThreshold));


        LayerDrawable temp = (LayerDrawable) this.getBackground();
        gradientDrawable = (GradientDrawable) temp.findDrawableByLayerId(R.id.part1);


        //Get colors
        kibo_background_color = resources.getColor(R.color.kibo_background);
        inactive_key_background_color = resources.getColor(R.color.inactive_key_background);
        inactive_key_text_color = resources.getColor(R.color.inactive_key_text);
        active_key_background_color = resources.getColor(R.color.active_key_background);
        active_key_text_color = resources.getColor(R.color.active_key_text);

        demoViewWidth = resources.getDimension(R.dimen.demoViewWidth);
        demoInputViewHeight = resources.getDimension(R.dimen.demoInputViewHeight);

    }

    public RoundedRectangleView(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);

        // Get resource values
        getResourceValuesAndSetup();

        instantiateHandler();

    }

    public RoundedRectangleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);

        // Get resource values
        getResourceValuesAndSetup();

        instantiateHandler();
    }

    public RoundedRectangleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);

        // Get resource values
        getResourceValuesAndSetup();

        instantiateHandler();
    }

    protected void setOnKiboActionListener(OnKiboActionListener listener) {
        mOnKiboActionListener = listener;
    }

    protected void setVibrator(Vibrator vibrator) {
        this.vibrator = vibrator;
    }

    protected void setTouchCoordinator(TouchCoordinator coordinator) {
        mTouchCoordinator = coordinator;
    }

    public class LetterAndCursorMoveRepeatingThread extends Thread {

        @Override
        public void run() {


            while(!interruptedLetterRepeatingThread) {
                try {

//                    System.out.print(delay);

                    synchronized (this) {
                        if (delay == -1) wait();
                    }

                    //Long press case
                    for (int i = 0; i < 7; i++) {
                        if (delay == -2) {
                            sleep(50);
                        } else break;
                    }
                    if (delay == -2) {
                        mHandler.sendEmptyMessage(0);
                        synchronized (this) {
                            wait();
                        }
                    }

                    //Letter/Cursor move repeating case
                    if (delay >= 0) {
                        sleep(delay);
                        if (delay >= 0) {
                            mHandler.sendEmptyMessage(0);
                        }
                    }


                } catch (InterruptedException ex) {

                }



            }
        }


    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param resources Resources to get device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(float dp, Resources resources){
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = (int)(dp * metrics.density + 0.5f);
        return px;
    }

    public static float convertMmToPixel(float mm, Resources resources){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, mm,
                resources.getDisplayMetrics());
    }
}