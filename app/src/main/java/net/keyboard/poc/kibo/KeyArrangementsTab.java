package net.keyboard.poc.kibo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Duy Dang on 3/4/2017.
 */

public class KeyArrangementsTab {

    HashMap<String,String[]> currentKeyArrangement = new HashMap();

    ArrayList<HashMap<String,String[]>> keyArrangements = new ArrayList<>();

    int currentIndex = 0;

    public void setCurrentKeyArrangement(int index) {
        currentKeyArrangement = keyArrangements.get(index);
        currentIndex = index;
    }

    public void getToNextKeyArrangement() {
        currentIndex = (currentIndex + 1) % keyArrangements.size();
        currentKeyArrangement = keyArrangements.get(currentIndex);
    }

    public void getToPreviousKeyArrangement() {
        if (currentIndex == 0) currentIndex = keyArrangements.size() - 1;
        else currentIndex = (currentIndex - 1) % keyArrangements.size();
        currentKeyArrangement = keyArrangements.get(currentIndex);
    }

}
