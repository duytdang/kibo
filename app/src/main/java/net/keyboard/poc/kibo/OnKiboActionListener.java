package net.keyboard.poc.kibo;

import android.content.SharedPreferences;
import android.view.WindowManager;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Duy Dang on 6/25/2016.
 */
// This interface is implemented by KiboIME in order to let Views inform KiboIME which letter to send away
public interface OnKiboActionListener {
    //public void onKey(int primaryCode, int[] keyCodes);
    void signal(String hashMapKey);

    void signal_nonpredict(String hashMapKey);

    boolean setSelection (int start, int end, Boolean singleCursorLeftward, Boolean leftCursorLeftward, Boolean rightCursorLeftWard);

    void getToNextKeyArrangementOfCurrentTab();

    void getToPreviousKeyArrangementOfCurrentTab();

    ExtractedText getExtractedText(ExtractedTextRequest req, int number);

    void changeColorOfRowToActive(int row);

    void changeColorOfAllToInactive();

    void resetRRWrapperBoundedViewsBackToNonPositionControlMode();

    void triggerEditorAction();

    void clearSuggestionViews();

    void resetAllViewsBackToNonPositionControlMode();

    void refreshAndResetWordPredViewsBackToNonPosContrlMode();

    boolean isAnyTextSelected();

    float getEmojiGroupViewBottomHeight();

    boolean isLettersTheCurrentTab();

    void saveEclipseBarsWrapperViewPositionsInSharedPreferences(String orientationString, float x, float y, float firstX_viewPosition_parentRelative, float firstY_viewPosition_parentRelative, float maxX, float maxY);

    SharedPreferences getSharedPreferences();

    String getScreenOrientation();

    float getWrapperX();
    float getWrapperY();

    float getMaxKiboAreaHeight();
    float getInitialKiboAreaHeight();

    void invertKiboPositionVertically();
}